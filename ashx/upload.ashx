﻿<%@ WebHandler Language="C#" Class="upload" %>

using System;
using System.Web;

public class upload : IHttpHandler {

    public void ProcessRequest(HttpContext context) {
        //try {
        HttpPostedFile file = context.Request.Files["myImg"];
            string uploadPath = "/upload/";
            if (file != null) {
                var server = HttpContext.Current.Server;
                string destdir = server.MapPath(uploadPath);
                
                if (!System.IO.Directory.Exists(destdir))
                    System.IO.Directory.CreateDirectory(destdir);//如果文件夹不存在就创建它

                char[] delimiterChars = { '.' };
                string[] arr = file.FileName.Split(delimiterChars);
                string ext = arr[arr.Length - 1];
                string newfile = DateTime.Now.ToString("yyyyMMddHHmmssfff") + "." + ext;
                file.SaveAs(destdir + newfile);
                string img_url = context.Request["img_url"] + "upload/";
                string json = "{\"img_url\":\"" + img_url + "\",\"file\":\"" + newfile + "\"}";
                context.Response.Write(json);
            }
            else {
                //MODEL.AjaxMsg m = new MODEL.AjaxMsg();
                //m.status = "error";
                //m.message = "文件没找到";
                //JavaScriptSerializer js = new JavaScriptSerializer();
                //context.Response.Write(js.Serialize(m));
                context.Response.Write("1");
            }
        //}
        //catch {
            //MODEL.AjaxMsg m = new MODEL.AjaxMsg();
            //m.status = "error";
            //m.message = "服务器错误";
            //JavaScriptSerializer js = new JavaScriptSerializer();
            //context.Response.Write(js.Serialize(m));
            //context.Response.Write("2");
        //}
    }

    public bool IsReusable {
        get {
            return false;
        }
    }

}