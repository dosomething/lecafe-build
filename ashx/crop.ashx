﻿<%@ WebHandler Language="C#" Class="crop" %>

using System;
using System.Web;
using System.Drawing;

public class crop : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {
        context.Response.ContentType = "text/plain";
        string imgurl = context.Request["imgUrl"];
        string filename = context.Request["filename"];
        int imgInitW = int.Parse(context.Request["imgInitW"].ToString());//原图宽
        int imgInitH = int.Parse(context.Request["imgInitH"].ToString());//原图长
        //缩放后图片长宽
        double HH = Convert.ToDouble(context.Request["imgH"].ToString());//可能在缩放时出现小数点
        int imgH = (int)HH;
        double WW = Convert.ToDouble(context.Request["imgW"].ToString());//可能在缩放时出现小数点
        int imgW = (int)WW;

        int imgY1 = int.Parse(context.Request["imgY1"].ToString());//剪切点起始坐标Y
        int imgX1 = int.Parse(context.Request["imgX1"].ToString());//剪切点起始坐标X
        int cropW = int.Parse(context.Request["cropW"].ToString());//剪切宽度
        int cropH = int.Parse(context.Request["cropH"].ToString());//剪切长度


        string cropPath = "/upload/";
        var server = HttpContext.Current.Server;
        string destdir = server.MapPath(cropPath);
        if (!System.IO.Directory.Exists(destdir))
            System.IO.Directory.CreateDirectory(destdir);


        var image = Image.FromFile(destdir + filename);
        var newImage = ScaleImage(image, imgW, imgH);
        image.Dispose();
        Bitmap target = new Bitmap(cropW, cropH);
        using (Graphics g = Graphics.FromImage(target)) {
            g.DrawImage(newImage, new Rectangle(0, 0, target.Width, target.Height),
                             new Rectangle(imgX1, imgY1, cropW, cropH),
                             GraphicsUnit.Pixel);
            g.Dispose();
            
        }
        target.Save(destdir + filename);
        target.Dispose();

        string json = "";
        json += "{\"status\":\"success\",\"url\":\"" + imgurl + "\"}";
        context.Response.Write(json);

        //string imgurl_t = cropPath + "Thumb_" + name;
        //App_Code.ImageHelp imgHelp = new App_Code.ImageHelp();
        //imgHelp.MakeThumNail(imgurl, imgurl_t, imgW, imgH, "W");
        //imgHelp.GetPart(imgurl_t, cropPath, 0, 0, cropW, cropH, imgX1, imgY1);
 
    }

    public static Image ScaleImage(Image image, int maxWidth, int maxHeight) {
        var ratioX = (double)maxWidth / image.Width;
        var ratioY = (double)maxHeight / image.Height;
        var ratio = Math.Min(ratioX, ratioY);

        var newWidth = (int)(image.Width * ratio);
        var newHeight = (int)(image.Height * ratio);

        var newImage = new Bitmap(newWidth, newHeight);

        using (var graphics = Graphics.FromImage(newImage))
            graphics.DrawImage(image, 0, 0, newWidth, newHeight);
        
        
        
        return newImage;
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}