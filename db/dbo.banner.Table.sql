/****** Object:  Table [dbo].[banner]    Script Date: 2015/8/26 下午 07:28:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[banner]') AND type in (N'U'))
DROP TABLE [dbo].[banner]
GO
/****** Object:  Table [dbo].[banner]    Script Date: 2015/8/26 下午 07:28:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[banner]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[banner](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[title] [nvarchar](255) NULL,
	[banner_type] [int] NULL,
	[value] [ntext] NULL,
	[starttime] [datetime] NULL,
	[endtime] [datetime] NULL,
	[publish] [int] NULL,
	[sort] [int] NULL,
 CONSTRAINT [PK_banner] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET IDENTITY_INSERT [dbo].[banner] ON 

INSERT [dbo].[banner] ([id], [title], [banner_type], [value], [starttime], [endtime], [publish], [sort]) VALUES (1, N'banner1', 0, N'{"pic":"20150826191500144.jpg","url":"https://www.facebook.com/","video":""}', CAST(N'2015-08-26 19:14:00.000' AS DateTime), CAST(N'2015-08-27 19:14:00.000' AS DateTime), 1, NULL)
SET IDENTITY_INSERT [dbo].[banner] OFF
