/****** Object:  Table [dbo].[site_meta]    Script Date: 2015/8/26 下午 07:28:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[site_meta]') AND type in (N'U'))
DROP TABLE [dbo].[site_meta]
GO
/****** Object:  Table [dbo].[site_meta]    Script Date: 2015/8/26 下午 07:28:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[site_meta]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[site_meta](
	[id] [int] NULL,
	[meta_name] [nvarchar](50) NULL,
	[meta_value] [nvarchar](255) NULL
) ON [PRIMARY]
END
GO
INSERT [dbo].[site_meta] ([id], [meta_name], [meta_value]) VALUES (NULL, N'site_title', N'左岸咖啡館')
INSERT [dbo].[site_meta] ([id], [meta_name], [meta_value]) VALUES (NULL, N'site_keyword', N'左岸咖啡館,左岸咖啡館,左岸咖啡館')
INSERT [dbo].[site_meta] ([id], [meta_name], [meta_value]) VALUES (NULL, N'site_description', N'左岸咖啡館,左岸咖啡館,左岸咖啡館')
INSERT [dbo].[site_meta] ([id], [meta_name], [meta_value]) VALUES (NULL, N'site_ogimage', N'20150826173922611.jpg')
