/****** Object:  Table [dbo].[admin_user]    Script Date: 2015/8/26 下午 07:28:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[admin_user]') AND type in (N'U'))
DROP TABLE [dbo].[admin_user]
GO
/****** Object:  Table [dbo].[admin_user]    Script Date: 2015/8/26 下午 07:28:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[admin_user]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[admin_user](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[username] [nvarchar](50) NULL,
	[password] [nvarchar](50) NULL,
	[publish] [int] NULL,
	[active] [int] NULL,
	[name] [nvarchar](50) NULL,
 CONSTRAINT [PK_admin_user] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET IDENTITY_INSERT [dbo].[admin_user] ON 

INSERT [dbo].[admin_user] ([id], [username], [password], [publish], [active], [name]) VALUES (1, N'oxox', N'b59c67bf196a4758191e42f76670ceba', 1, 1, N'管理者')
INSERT [dbo].[admin_user] ([id], [username], [password], [publish], [active], [name]) VALUES (2, N'aaa', N'47bce5c74f589f4867dbd57e9ca9f808', 0, 0, N'aaa')
SET IDENTITY_INSERT [dbo].[admin_user] OFF
