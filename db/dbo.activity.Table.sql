/****** Object:  Table [dbo].[activity]    Script Date: 2015/8/26 下午 07:28:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[activity]') AND type in (N'U'))
DROP TABLE [dbo].[activity]
GO
/****** Object:  Table [dbo].[activity]    Script Date: 2015/8/26 下午 07:28:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[activity]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[activity](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[title] [nvarchar](255) NULL,
	[starttime] [datetime] NULL,
	[endtime] [datetime] NULL,
	[value] [ntext] NULL,
	[publish] [int] NULL,
	[sort] [int] NULL,
 CONSTRAINT [PK_activity] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET IDENTITY_INSERT [dbo].[activity] ON 

INSERT [dbo].[activity] ([id], [title], [starttime], [endtime], [value], [publish], [sort]) VALUES (1, N'test', CAST(N'2015-07-01 17:50:00.000' AS DateTime), CAST(N'2015-08-27 17:50:00.000' AS DateTime), N'{"pic":"20150826180949513.jpg","url":"https://www.google.com.tw/webhp?hl=zh-TW"}', 1, NULL)
SET IDENTITY_INSERT [dbo].[activity] OFF
