/****** Object:  Table [dbo].[video]    Script Date: 2015/8/26 下午 07:28:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[video]') AND type in (N'U'))
DROP TABLE [dbo].[video]
GO
/****** Object:  Table [dbo].[video]    Script Date: 2015/8/26 下午 07:28:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[video]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[video](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[title] [nvarchar](255) NULL,
	[starttime] [datetime] NULL,
	[endtime] [datetime] NULL,
	[value] [ntext] NULL,
	[publish] [int] NULL,
	[sort] [int] NULL,
 CONSTRAINT [PK_video] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET IDENTITY_INSERT [dbo].[video] ON 

INSERT [dbo].[video] ([id], [title], [starttime], [endtime], [value], [publish], [sort]) VALUES (13, N'1', CAST(N'2015-08-26 18:56:00.000' AS DateTime), CAST(N'2015-08-27 18:56:00.000' AS DateTime), N'{"txt":"adasdasd","video":"<iframe width=\"640\" height=\"360\" src=\"https://www.youtube.com/embed/Iyu909SyWsY?rel=0&amp;controls=0&amp;showinfo=0\" frameborder=\"0\" allowfullscreen></iframe>"}', 1, NULL)
SET IDENTITY_INSERT [dbo].[video] OFF
