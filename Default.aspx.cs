﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

public partial class _Default : PageBase {
    public DataRow dr_meta;
    public DataTable dt_banner;
    public DataTable dt_product;
    public DataTable dt_video;
    public DataTable dt_activity;
    protected void Page_Load(object sender, EventArgs e)
    {
        SiteMeta ctrl_meta = new SiteMeta(dbconn);
        dr_meta = ctrl_meta.get();

        Banner ctrl_banner = new Banner(dbconn);
        dt_banner = ctrl_banner.getList_front();
        rpt_banner.DataSource = dt_banner;
        rpt_banner.DataBind();

        Product ctrl_product = new Product(dbconn);
        dt_product = ctrl_product.getList_front();
        rpt_product_a.DataSource = rpt_product_b.DataSource = dt_product;
        rpt_product_a.DataBind();
        rpt_product_b.DataBind();

        Video ctrl_video = new Video(dbconn);
        dt_video = ctrl_video.getList_front();
        rpt_video_a.DataSource = rpt_video_b.DataSource = dt_video;
        rpt_video_a.DataBind();
        rpt_video_b.DataBind();

        Activity ctrl_activity = new Activity(dbconn);
        dt_activity = ctrl_activity.getList_front();
        rpt_activity.DataSource = dt_activity;
        rpt_activity.DataBind();


    }

    public string json_val(string json,string key) {
        JToken data = JsonConvert.DeserializeObject<JObject>(json);
        return HttpUtility.UrlDecode(data[key].ToString()).Replace(@"\", string.Empty); ;
    }
}