﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>
<html lang="zh-TW">
<head>
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport">
    <link href="asset/images/favicon.png" rel="shortcut icon" type="image/png">
    <title><%=dr_meta["site_title"] %>
    </title>
    <meta content="<%=dr_meta["site_keyword"] %>" name="keywords">
    <meta content="<%=dr_meta["site_description"] %>" name="description">
    <meta content="<%=dr_meta["site_title"] %>" property="og:title">
    <meta content="<%=dr_meta["site_title"] %>" property="og:site_name">
    <meta content="/" property="og:url">
    <meta content="<%=baseURL+"upload/"+dr_meta["site_ogimage"] %>" property="og:image">
    <meta content="<%=dr_meta["site_description"] %>" property="og:description">
    <meta content="website" property="og:type">
    <meta content="" property="fb:admins">
    <link href="asset/css/all.css" media="screen" rel="stylesheet" type="text/css">
    <!--[if lt IE 10]>
    <link href="asset/css/ie.css" media="screen" rel="stylesheet" type="text/css">
    <![endif]-->
</head>
<body class="is-loading">
    <!--[if lt IE 10]>
    <div class="ie-message">
      <div class="inner">
        <div class="logo">
          <img alt="左岸咖啡館" src="asset/images/icon-logo.png">
        </div>
        <div class="title">
          <p class="h2">
            我們偵測到您使用的瀏覽器版本過舊
          </p>
          <p>
            為了能有更舒適的瀏覽環境<br/>建議您<a href="http://windows.microsoft.com/zh-tw/internet-explorer/download-ie" target=_blank>更新</a>您的瀏覽器版本或使用 <a href="https://www.google.com.tw/chrome/browser/desktop/" target=_blank>Google Chrome</a>, <a href="https://www.mozilla.org/zh-TW/firefox/new/" target=_blank>Firefox</a> 瀏覽器
          </p>
        </div>
      </div>
    </div>
    <![endif]-->
    <div class="loading">
        <div class="logo"></div>
    </div>
    <div class="header">
        <div class="logo" data-menuanchor="Home">
            <a href="#Home">左岸咖啡館</a>
        </div>
        <div class="main-nav">
            <div class="share-facebook">
                <a href="https://www.facebook.com/lecafe.tw" target="_blank"></a>
            </div>
            <div class="nav">
                <ul id="mainNav">
                    <li data-menuanchor="Home">
                        <a href="#Home">左岸咖啡館</a>
                    </li>
                    <li data-menuanchor="Products">
                        <a href="#Products">嗜左岸</a>
                    </li>
                    <li data-menuanchor="Video">
                        <a href="#Video">法式電影藝廊</a>
                    </li>
                    <li data-menuanchor="Event">
                        <a href="#Event">浪漫活動時光</a>
                    </li>
                </ul>
            </div>
            <div class="toggle">
                <a href="#">
                    <div class="toggle-text">
                        MENU
                    </div>
                    <div class="toggle-icon">
                        <span></span>
                    </div>
                </a>
            </div>
        </div>
    </div>
    <div class="page">
        <section class="fullscreen">
            <div class="fullscreen-img">
                <ul class="bxSlide-index">
                    <asp:Repeater ID="rpt_banner" runat="server">
                        <ItemTemplate>
                            <li>
                                <%#(Eval("banner_type").ToString()=="0")?
                                ("<a href=\"" + (json_val(Eval("value").ToString(), "url") == "" ? "#" : json_val(Eval("value").ToString(), "url")) + "\" target=\"_blank\"><img src=\"upload/" + json_val(Eval("value").ToString(),"pic") + "\">" + (Eval("title").ToString()==""?"":"<div class=\"slide-text\"><p>" + Eval("title") + "</p></div>") + "</a>"):
                                "<div class=\"embed-responsive\"><img class=\"ratio\" src=\"asset/images/bg_169.png\"><div class=\"youtube-video\" id=\""+json_val(Eval("value").ToString(), "video") + "\"></div></div>"
                                    
                                %>
                                <%--<a href="<%#json_val(Eval("value").ToString(), "url") == "" ? "#" : json_val(Eval("value").ToString(), "url") %>">
                                    <img alt="" src="upload/<%#json_val(Eval("value").ToString(),"pic") %>">
                                    <%#(Eval("title").ToString()==""?"":"<div class=\"slide-text\"><p>" + Eval("title") + "</p></div>") %>
                                </a>--%>
                            </li>
                        </ItemTemplate>
                    </asp:Repeater>
                </ul>
            </div>
            <div class="mouse-wheel">
                <div class="wheel"></div>
            </div>
        </section>
        <section class="products">
            <div class="product-list">
                <div class="product-block">

                    <div class="title">
                        <h1>Coffee
                            <p>嗜左岸</p>
                        </h1>
                    </div>
                    <div class="product-slide">
                        <ul class="bxSlide-product">
                            <asp:Repeater ID="rpt_product_a" runat="server">
                                <ItemTemplate>
                                    <li>
                                        <img alt="<%#Eval("title") %>" src="upload/<%#json_val(Eval("value").ToString(),"cover") %>">
                                        <div class="subject">
                                            <%#Eval("title") %>
                                        </div>
                                        <div class="text">
                                            <%#json_val(Eval("value").ToString(),"txt") %>
                                        </div>
                                    </li>

                                </ItemTemplate>
                            </asp:Repeater>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="product-image">
                <ul class="bxSlide-product-image">
                    <asp:Repeater ID="rpt_product_b" runat="server">
                        <ItemTemplate>
                            <li style="background-image: url(upload/<%#json_val(Eval("value").ToString(),"image") %>)"></li>
                        </ItemTemplate>
                    </asp:Repeater>
                </ul>
            </div>
            <div class="mouse-wheel">
                <div class="wheel"></div>
            </div>
        </section>
        <section class="film">
            <div class="title">
                <h1>Video
                    <p>法式電影藝廊</p>
                </h1>
            </div>
            <div class="video-area container">
                <div class="video-list">
                    <ul class="bxSlide-video">
                        <asp:Repeater ID="rpt_video_a" runat="server">
                            <ItemTemplate>
                                <li>
                                    <div class="video">
                                        <%#json_val(Eval("value").ToString(),"video") %>
                                    </div>
                                    <div class="info">
                                        <div class="name">
                                            <%#Eval("title") %>
                                        </div>
                                        <div class="descripition">
                                            <%#json_val(Eval("value").ToString(),"txt") %>
                                        </div>
                                    </div>
                                </li>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ul>
                </div>
                <div class="video-menu">
                    <ul>
                        <asp:Repeater ID="rpt_video_b" runat="server">
                            <ItemTemplate>
                                <li>
                                    <a href="#">
                                        <div class="name">
                                            <%#Eval("title") %>
                                        </div>
                                        <div class="descripition">
                                            <%#json_val(Eval("value").ToString(),"txt") %>
                                        </div>
                                    </a>
                                </li>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ul>
                </div>
            </div>
            <div class="mouse-wheel">
                <div class="wheel"></div>
            </div>
        </section>
        <section class="event">
            <div class="title">
                <h1>Event
                    <p>浪漫活動時光</p>
                </h1>
            </div>
            <div class="event-list">
                <ul class="bxSlide-event">
                    <asp:Repeater ID="rpt_activity" runat="server">
                        <ItemTemplate>
                            <li>
                                <a href="<%#json_val(Eval("value").ToString(),"url") %>" target="_blank">
                                    <div class="img">
                                        <img alt="" src="upload/<%#json_val(Eval("value").ToString(),"pic") %>">
                                    </div>
                                    <div class="descripition">
                                        <div class="subject">
                                            <%#Eval("title") %>
                                        </div>
                                        <div class="date">
                                            <%#Convert.ToDateTime(Eval("starttime").ToString()).ToString("yyyy.MM") %>
                                        </div>
                                    </div>
                                </a>
                            </li>
                        </ItemTemplate>
                    </asp:Repeater>
                </ul>
            </div>
            <div class="event-timeline">
                <div id="timeline-list"></div>
            </div>
            <div class="footer">
                <div class="copyright">
                    <div class="text">
                        <p>
                            本網站內容均受著作權法保障，除非有特別聲明，其著作權屬於統一企業(股)公司或其他內容提供者所有，請勿將全部或部分圖文內容轉載於任何形式媒體
                        </p>
                        <span>Copyright © 2015  Uni-President Enterprises Corp., All rights reserved.</span>
                    </div>
                </div>
                <div class="mobile-logo">
                    <div class="uni-president"></div>
                    <div class="presco"></div>
                </div>
            </div>
        </section>
    </div>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript"></script>
    <script src="asset/vendor/modernizr.js" type="text/javascript"></script>
    <script src="asset/vendor/jpreloader.min.js" type="text/javascript"></script>
    <script src="asset/vendor/jquery.easings.min.js" type="text/javascript"></script>
    <script src="asset/vendor/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="asset/vendor/jquery.fullPage.min.js" type="text/javascript"></script>
    <script src="asset/vendor/jquery.bxslider.min.js" type="text/javascript"></script>
    <script src="asset/vendor/mediaelement-and-player.min.js" type="text/javascript"></script>
    <script src="asset/js/all.js" type="text/javascript"></script>
</body>
</html>
