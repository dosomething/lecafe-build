﻿using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Data.SqlClient;
using System.Web;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Text.RegularExpressions;

/// <summary>
/// DataAccess 的摘要描述
/// </summary>
public class DataAccess
{
    public SqlConnection dbconn;
    public DataAccess(SqlConnection dbconn)
	{
		//
		// TODO: 在這裡新增建構函式邏輯
		//
        this.dbconn = dbconn;
	}

    public DataSet query(StringBuilder sql) {
        DataSet ds = new DataSet();
        SqlDataAdapter adapter = new SqlDataAdapter(sql.ToString(), dbconn);
        adapter.Fill(ds);
        return ds;
    }

    public void nonQuery(StringBuilder sql) {
        SqlCommand cmd;
        try {
            string strSQL = sql.ToString();
            cmd = new SqlCommand(strSQL, dbconn);
            cmd.ExecuteNonQuery();
        }
        catch (Exception Ex) {
            // handle any exception here
        }

    }

    public void nonQuery2(SqlCommand cmd) {
        try {
            cmd.Connection = dbconn;
            //string strSQL = sql.ToString();
            //cmd = new SqlCommand(strSQL, dbconn);
            cmd.ExecuteNonQuery();
        }
        catch (Exception Ex) {
            // handle any exception here
        }

    }
}