﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

/// <summary>
/// PageBase 的摘要描述
/// </summary>
public class PageBase : System.Web.UI.Page {

	protected string basePath = "";
    protected string baseURL = "";
    protected string loginSessionKey = "_login_user_key_";
    protected string memberSessionKey = "_login_member_key_";
	protected string defaultLoginPage = "login.aspx";
	protected SqlConnection dbconn;
	private string script_name;
	//private string connectionString = "";
	protected string site_title = "GCDF";

	/// <summary>
	/// 所有前端頁面 不需要登入 的通通繼承此物件
	/// </summary>
	public PageBase() {
		//
		// TODO: 在此加入建構函式的程式碼
		//
        baseURL = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority + HttpContext.Current.Request.ApplicationPath.TrimEnd('/') + "/"; ;

		try {

			script_name = HttpContext.Current.Request.ServerVariables["SCRIPT_NAME"];
			Logger.WriteLog("SCRIPT NAME : " + script_name + " begin");

			// 網站最原始路徑
			basePath = Server.MapPath("~/");

			// 建立資料庫連線
			string connectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
			dbconn = new SqlConnection(connectionString);
			dbconn.Open();
			Logger.WriteLog("SCRIPT NAME : " + script_name + " get connection ");

		} catch (Exception ex) {
			Logger.WriteLog(ex.ToString());
			//MyRedirect("~/GenericErrorPage.aspx", true);
            //Response.Write(ex.ToString());
			return;
		}

	}


	public void Page_Unload(object sender, EventArgs e) {

		try {
			// 結束物件之前，常是關閉資料庫連線
			dbconn.Close();
			dbconn.Dispose();
			Logger.WriteLog("SCRIPT NAME : " + script_name + " connection closed");
		} catch {
		}
	}

	public void MyRedirect(string url, bool endResponse) {
		try {
			HttpContext.Current.Response.Redirect(url, true);
			Logger.WriteLog("redirect to url " + url);
		} catch (Exception ex) {
			//Logger.WriteLog(ex.ToString());
		}
	}


	/// <summary>
	/// 網址導向
	/// </summary>
	/// <param name="errMsg"></param>
	/// <param name="goURL"></param>
	public void gotoMsgURL(string errMsg, string goURL) {

		try {
			string msg = "";

			msg += "<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">";
			msg += "<script>\n";

			if (errMsg != "")
				msg += "alert(\"" + Regex.Replace(errMsg, "\"", "\\\"") + "\");\n";

			if (string.IsNullOrEmpty(goURL))
				msg += "history.go(-1);\n";
			else {
				if (goURL.IndexOf("?") > 0)
                    msg += "top.location.href=\"" + goURL + "&" + HttpContext.Current.Request.RawUrl + "\";\n";
				else
                    msg += "top.location.href=\"" + goURL + "?path=" + HttpContext.Current.Request.RawUrl + "\";\n";
			}

			msg += "</script></html>\n";
			HttpContext.Current.Response.Write(msg);
			HttpContext.Current.Response.End();
			Logger.WriteLog("redirect to url " + goURL);

		} catch (Exception ex) {

		}


	}


}
