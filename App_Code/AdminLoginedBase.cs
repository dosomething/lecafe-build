﻿using System;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.Odbc;
using System.Text.RegularExpressions;
/// <summary>
/// AdminLoginedBase 的摘要描述
/// </summary>
public class AdminLoginedBase : PageBase
{
	public AdminLoginedBase()
	{
		//
		// TODO: 在這裡新增建構函式邏輯
		//
        Load += new EventHandler(CheckAdminLogined);
	}
    protected void CheckAdminLogined(object sender, EventArgs e) {
        if (Session[loginSessionKey] == null || String.IsNullOrEmpty(Session[loginSessionKey].ToString())) {
            gotoMsgURL("使用時間逾期,請重新登入!!", "/admin");
        }
    }
}