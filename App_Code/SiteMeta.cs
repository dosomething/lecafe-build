﻿using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Data.SqlClient;
using System.Web;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

/// <summary>
/// SiteMeta 的摘要描述
/// </summary>
public class SiteMeta : DataAccess
{
    public SiteMeta(SqlConnection dbconn)
        : base(dbconn) {
        //
        // TODO: 在這裡新增建構函式邏輯
        //
        this.dbconn = dbconn;
    }

    public DataRow get() {
        DataRow drow;
        StringBuilder sql = new StringBuilder();
        sql.AppendFormat(@"select 
            (select meta_value from site_meta where meta_name='site_title') as site_title,
            (select meta_value from site_meta where meta_name='site_keyword') as site_keyword,
            (select meta_value from site_meta where meta_name='site_description') as site_description,
            (select meta_value from site_meta where meta_name='site_ogimage') as site_ogimage");
        DataSet ds = query(sql);
        drow = ds.Tables[0].Rows[0];
        
        return drow;
    }

    public void save() {

        string site_title = HttpContext.Current.Request.Form["site_title"];
        string site_keyword = HttpContext.Current.Request.Form["site_keyword"];
        string site_description = HttpContext.Current.Request.Form["site_description"];
        string site_ogimage = HttpContext.Current.Request.Form["site_ogimage"];

        StringBuilder sql = new StringBuilder();
        sql.AppendFormat("delete from site_meta");
        nonQuery(sql);

        sql = new StringBuilder();
        sql.AppendFormat("insert into site_meta(meta_name,meta_value) values('{0}','{1}');", "site_title", site_title);
        sql.AppendFormat("insert into site_meta(meta_name,meta_value) values('{0}','{1}');", "site_keyword", site_keyword);
        sql.AppendFormat("insert into site_meta(meta_name,meta_value) values('{0}','{1}');", "site_description", site_description);
        sql.AppendFormat("insert into site_meta(meta_name,meta_value) values('{0}','{1}');", "site_ogimage", site_ogimage);
        nonQuery(sql);
    }
}