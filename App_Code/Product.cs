﻿using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Data.SqlClient;
using System.Web;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

/// <summary>
/// Product 的摘要描述
/// </summary>
public class Product : DataAccess
{
    string table_name = "product";

    public Product(SqlConnection dbconn)
        : base(dbconn) {
        //
        // TODO: 在這裡新增建構函式邏輯
        //
        this.dbconn = dbconn;
    }

    public DataTable getList_front() {
        StringBuilder sql = new StringBuilder();
        sql.AppendFormat(@"select * from {0} where publish=1 and 
            CONVERT(datetime,'{1}') between starttime and endtime order by sort,id desc",
            table_name,
            DateTime.Now.ToString("yyyy/MM/dd HH:mm:") + "01");
        DataSet ds = query(sql);
        DataTable dt = ds.Tables[0];
        return dt;
    }

    public DataTable getList() {
        StringBuilder sql = new StringBuilder();
        sql.AppendFormat("select * from {0} order by sort,id desc", table_name);
        DataSet ds = query(sql);
        DataTable dt = ds.Tables[0];
        return dt;
    }

    public DataRow getRow() {
        string id = HttpContext.Current.Request.QueryString["id"];
        StringBuilder sql = new StringBuilder();
        sql.AppendFormat("select * from {0} where id='{1}'", table_name, id);
        DataSet ds = query(sql);
        DataRow row;
        if (ds.Tables[0].Rows.Count > 0) {
            row = ds.Tables[0].Rows[0];
        }
        else {
            DataTable dt = new DataTable();
            dt.Columns.Add("id");
            dt.Columns.Add("title");
            dt.Columns.Add("value");
            dt.Columns.Add("starttime");
            dt.Columns.Add("endtime");
            dt.Columns.Add("publish");

            row = dt.NewRow();
            row["id"] = 0;
            row["title"] = "";
            row["value"] = "{}";
            row["starttime"] = DateTime.Now.ToString("yyyy/MM/dd HH:mm");
            row["endtime"] = DateTime.Now.ToString("yyyy/MM/dd HH:mm");
            row["publish"] = 0;
        }

        return row;
    }

    public void action() {
        string method = HttpContext.Current.Request.Form["method"];
        switch (method) {
            case "save":
                do_save();
                break;
            case "publish":
                do_publish();
                break;
            case "delete":
                do_delete();
                break;
            case "sort":
                do_sort();
                break;
        }
    }

    private void do_save() {
        string id = HttpContext.Current.Request.Form["id"];
        string title = HttpContext.Current.Request.Form["title"];
        string banner_type = HttpContext.Current.Request.Form["userpwd"];
        string starttime = HttpContext.Current.Request.Form["starttime"];
        string endtime = HttpContext.Current.Request.Form["endtime"];
        string value = HttpContext.Current.Request.Form["value"];
        string publish = HttpContext.Current.Request.Form["publish"];

        if (id == "0") {
            //insert
            StringBuilder sql = new StringBuilder();
            sql.AppendFormat("insert into {0}(title,starttime,endtime,value,publish) values('{1}','{2}','{3}','{4}','{5}')",
                table_name,
                title.Replace("'", "''"),
                starttime,
                endtime,
                value.Replace("'", "''"),
                publish
                );
            nonQuery(sql);
            HttpContext.Current.Response.Write("alert('儲存完成');location='default.aspx';");
        }
        else {
            //update
            StringBuilder sql = new StringBuilder();
            sql.AppendFormat("update {0} set ", table_name);
            sql.AppendFormat("title='{0}',", title.Replace("'", "''"));
            sql.AppendFormat("starttime='{0}',", starttime);
            sql.AppendFormat("endtime='{0}',", endtime);
            sql.AppendFormat("value='{0}',", value.Replace("'", "''"));
            sql.AppendFormat("publish='{0}'", publish);
            sql.AppendFormat(" where id='{0}'", id);
            nonQuery(sql);
            HttpContext.Current.Response.Write("alert('儲存完成');location.reload();");
        }
    }

    private void do_publish() {
        string id = HttpContext.Current.Request.Form["id"];
        string publish = HttpContext.Current.Request.Form["publish"];
        StringBuilder sql = new StringBuilder();
        sql.AppendFormat("update {0} set publish='{1}' where id='{2}'", table_name, publish, id);
        nonQuery(sql);
    }

    private void do_delete() {
        string id = HttpContext.Current.Request.Form["id"];
        StringBuilder sql = new StringBuilder();
        sql.AppendFormat("delete from {0} where id='{1}'", table_name, id);
        nonQuery(sql);
        HttpContext.Current.Response.Write("alert('刪除完成');$('#row_" + id + "').remove();");
    }

    private void do_sort() {
        string[] ids = HttpContext.Current.Request.Form.GetValues("id");
        StringBuilder sql = new StringBuilder();
        sql.AppendFormat("update {0} set sort= case id ", table_name);
        for (int i = 0; i < ids.Length; i++) {
            sql.AppendFormat("when {0} then {1}", ids[i], i);
            sql.AppendFormat("{0}", (i < ids.Length - 1) ? "" : " end");
        }
        nonQuery(sql);
        HttpContext.Current.Response.Write(sql);
    }
}