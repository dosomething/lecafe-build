﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// PageBaseLoginedAjax 的摘要描述
/// </summary>
public class PageBaseLoginedAjax : PageBase
{
	public PageBaseLoginedAjax()
	{
		//
		// TODO: 在這裡新增建構函式邏輯
        //
        Load += new EventHandler(CheckUserLogined);
	}
    protected void CheckUserLogined(object sender, EventArgs e) {
        if (Session[memberSessionKey] == null || String.IsNullOrEmpty(Session[memberSessionKey].ToString())) {
            HttpContext.Current.Response.Write("alert('請重新登入會員!!');top.location='/login.aspx'");
            HttpContext.Current.Response.End();
        }
    }
}