﻿using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Data.SqlClient;
using System.Web;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

/// <summary>
/// Account 的摘要描述
/// </summary>
public class Account : DataAccess
{
    public Account(SqlConnection dbconn)
        : base(dbconn)
	{
		//
		// TODO: 在這裡新增建構函式邏輯
		//
        this.dbconn = dbconn;
	}

    public int account_login() {
        string username = HttpContext.Current.Request.Form["username"];
        string userpwd = HttpContext.Current.Request.Form["password"];
        StringBuilder sql = new StringBuilder();
        sql.AppendFormat("select * from admin_user where username='{0}' and password='{1}' and active=1 and publish=1", username, WebUtil.md5Encode(userpwd));
        DataSet ds = query(sql);
        //HttpContext.Current.Response.Write(sql.ToString());
        if (ds.Tables.Count == 0) {
            return 0;
        }

        if (ds.Tables[0].Rows.Count == 0) {
            return 0;
        }

        return int.Parse(ds.Tables[0].Rows[0]["id"].ToString());
    }

    public DataTable getList() {
        StringBuilder sql = new StringBuilder();
        sql.AppendFormat("select * from admin_user where active='1'");
        DataSet ds = query(sql);
        DataTable dt = ds.Tables[0];
        return dt;
    }

    public DataRow getRow() {
        string id = HttpContext.Current.Request.QueryString["id"];
        StringBuilder sql = new StringBuilder();
        sql.AppendFormat("select * from admin_user where id='{0}' and active=1",id);
        DataSet ds = query(sql);
        DataRow row;
        if (ds.Tables[0].Rows.Count > 0) {
            row = ds.Tables[0].Rows[0];
        }
        else {
            DataTable dt = new DataTable();
            dt.Columns.Add("id");
            dt.Columns.Add("username");
            dt.Columns.Add("password");
            dt.Columns.Add("publish");
            dt.Columns.Add("name");

            row = dt.NewRow();
            row["id"] = 0;
            row["username"] = "";
            row["publish"] = 0;
            row["name"] = "";
        }
        
        return row;
    }

    public void action() {
        string method = HttpContext.Current.Request.Form["method"];
        switch (method) {
            case "save":
                do_save();
                break;
            case "publish":
                do_publish();
                break;
            case "delete":
                do_delete();
                break;
        }
    }

    private void do_save() {
        string id = HttpContext.Current.Request.Form["id"];
        string username = HttpContext.Current.Request.Form["username"];
        string userpwd = HttpContext.Current.Request.Form["userpwd"];
        string name = HttpContext.Current.Request.Form["name"];
        string publish = HttpContext.Current.Request.Form["publish"];

        if (id == "0") {
            //insert
            StringBuilder sql = new StringBuilder();
            sql.AppendFormat("insert into admin_user(username,password,publish,name,active) values('{0}','{1}','{2}','{3}',1)",
                username,
                WebUtil.md5Encode(userpwd),
                publish,
                name
                );
            DataSet ds = query(sql);
            HttpContext.Current.Response.Write("alert('儲存完成');location='default.aspx';");
        }
        else {
            //update
            StringBuilder sql = new StringBuilder();
            sql.AppendFormat("update admin_user set ");
            if (userpwd != "") {
                sql.AppendFormat("password='{0}',", WebUtil.md5Encode(userpwd));
            }
            sql.AppendFormat("name='{0}',", name);
            sql.AppendFormat("publish='{0}'", publish);
            sql.AppendFormat(" where id='{0}'", id);
            DataSet ds = query(sql);
            HttpContext.Current.Response.Write("alert('儲存完成');location.reload();");
        }
    }

    private void do_publish() {
        string id = HttpContext.Current.Request.Form["id"];
        string publish = HttpContext.Current.Request.Form["publish"];
        StringBuilder sql = new StringBuilder();
        sql.AppendFormat("update admin_user set publish='{0}' where id='{1}'", publish, id);
        nonQuery(sql);
    }

    private void do_delete() {
        string id = HttpContext.Current.Request.Form["id"];
        StringBuilder sql = new StringBuilder();
        sql.AppendFormat("update admin_user set active='0' where id='{0}'", id);
        nonQuery(sql);
        HttpContext.Current.Response.Write("alert('刪除完成');$('#row_"+id+"').remove();");
    }
}