﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Text.RegularExpressions;

/// <summary>
/// validatorUtil 的摘要描述
/// </summary>
public class validatorUtil
{
	public validatorUtil()
	{
		//
		// TODO: 在此加入建構函式的程式碼
		//
	}

	/// <summary>
    /// 判斷字串是否與指定正則運算式匹配
    /// </summary>
    /// <paramItems name="input">要驗證的字串</paramItems>
    /// <paramItems name="regularExp">正則運算式</paramItems>
    /// <returns>驗證通過返回true</returns>
    public static bool IsMatch(string input, string regularExp)
    {
        return Regex.IsMatch(input, regularExp);
    }

    /// <summary>
    /// 驗證非負整數（正整數 + 0）
    /// </summary>
    /// <paramItems name="input">要驗證的字串</paramItems>
    /// <returns>驗證通過返回true</returns>
    public static bool IsUnMinusInt(string input)
    {
        return Regex.IsMatch(input, RegularExp.UnMinusInteger);
    }

    /// <summary>
    /// 驗證正整數
    /// </summary>
    /// <paramItems name="input">要驗證的字串</paramItems>
    /// <returns>驗證通過返回true</returns>
    public static bool IsPlusInt(string input)
    {
        return Regex.IsMatch(input, RegularExp.PlusInteger);
    }

    /// <summary>
    /// 驗證非正整數（負整數 + 0） 
    /// </summary>
    /// <paramItems name="input">要驗證的字串</paramItems>
    /// <returns>驗證通過返回true</returns>
    public static bool IsUnPlusInt(string input)
    {
        return Regex.IsMatch(input, RegularExp.UnPlusInteger);
    }

    /// <summary>
    /// 驗證負整數
    /// </summary>
    /// <paramItems name="input">要驗證的字串</paramItems>
    /// <returns>驗證通過返回true</returns>
    public static bool IsMinusInt(string input)
    {
        return Regex.IsMatch(input, RegularExp.MinusInteger);
    }

    /// <summary>
    /// 驗證整數
    /// </summary>
    /// <paramItems name="input">要驗證的字串</paramItems>
    /// <returns>驗證通過返回true</returns>
    public static bool IsInt(string input)
    {
        return Regex.IsMatch(input, RegularExp.Integer);
    }

    /// <summary>
    /// 驗證非負浮點數（正浮點數 + 0）
    /// </summary>
    /// <paramItems name="input">要驗證的字串</paramItems>
    /// <returns>驗證通過返回true</returns>
    public static bool IsUnMinusFloat(string input)
    {
        return Regex.IsMatch(input, RegularExp.UnMinusFloat);
    }

    /// <summary>
    /// 驗證正浮點數
    /// </summary>
    /// <paramItems name="input">要驗證的字串</paramItems>
    /// <returns>驗證通過返回true</returns>
    public static bool IsPlusFloat(string input)
    {
        return Regex.IsMatch(input, RegularExp.PlusFloat);
    }

    /// <summary>
    /// 驗證非正浮點數（負浮點數 + 0）
    /// </summary>
    /// <paramItems name="input">要驗證的字串</paramItems>
    /// <returns>驗證通過返回true</returns>
    public static bool IsUnPlusFloat(string input)
    {
        return Regex.IsMatch(input, RegularExp.UnPlusFloat);
    }

    /// <summary>
    /// 驗證負浮點數
    /// </summary>
    /// <paramItems name="input">要驗證的字串</paramItems>
    /// <returns>驗證通過返回true</returns>
    public static bool IsMinusFloat(string input)
    {
        return Regex.IsMatch(input, RegularExp.MinusFloat);
    }

    /// <summary>
    /// 驗證浮點數
    /// </summary>
    /// <paramItems name="input">要驗證的字串</paramItems>
    /// <returns>驗證通過返回true</returns>
    public static bool IsFloat(string input)
    {
        return Regex.IsMatch(input, RegularExp.Float);
    }

    /// <summary>
    /// 驗證由26個英文字母組成的字串
    /// </summary>
    /// <paramItems name="input">要驗證的字串</paramItems>
    /// <returns>驗證通過返回true</returns>
    public static bool IsLetter(string input)
    {
        return Regex.IsMatch(input, RegularExp.Letter);
    }

    /// <summary>
    /// 驗證由中文組成的字串
    /// </summary>
    /// <paramItems name="input">要驗證的字串</paramItems>
    /// <returns>驗證通過返回true</returns>
    public static bool IsChinese(string input)
    {
        return Regex.IsMatch(input, RegularExp.Chinese);
    }

    /// <summary>
    /// 驗證由26個英文字母的大寫組成的字串
    /// </summary>
    /// <paramItems name="input">要驗證的字串</paramItems>
    /// <returns>驗證通過返回true</returns>
    public static bool IsUpperLetter(string input)
    {
        return Regex.IsMatch(input, RegularExp.UpperLetter);
    }

    /// <summary>
    /// 驗證由26個英文字母的小寫組成的字串
    /// </summary>
    /// <paramItems name="input">要驗證的字串</paramItems>
    /// <returns>驗證通過返回true</returns>
    public static bool IsLowerLetter(string input)
    {
        return Regex.IsMatch(input, RegularExp.LowerLetter);
    }

    /// <summary>
    /// 驗證由數位和26個英文字母組成的字串
    /// </summary>
    /// <paramItems name="input">要驗證的字串</paramItems>
    /// <returns>驗證通過返回true</returns>
    public static bool IsNumericOrLetter(string input)
    {
        return Regex.IsMatch(input, RegularExp.NumericOrLetter);
    }

    /// <summary>
    /// 驗證由數位組成的字串
    /// </summary>
    /// <paramItems name="input">要驗證的字串</paramItems>
    /// <returns>驗證通過返回true</returns>
    public static bool IsNumeric(string input)
    {
        return Regex.IsMatch(input, RegularExp.Numeric);
    }
    /// <summary>
    /// 驗證由數位和26個英文字母或中文組成的字串
    /// </summary>
    /// <paramItems name="input">要驗證的字串</paramItems>
    /// <returns>驗證通過返回true</returns>
    public static bool IsNumericOrLetterOrChinese(string input)
    {
        return Regex.IsMatch(input, RegularExp.NumbericOrLetterOrChinese);
    }
    /// <summary>
    /// 驗證由數位、26個英文字母或者下劃線組成的字串
    /// </summary>
    /// <paramItems name="input">要驗證的字串</paramItems>
    /// <returns>驗證通過返回true</returns>
    public static bool IsNumericOrLetterOrUnderline(string input)
    {
        return Regex.IsMatch(input, RegularExp.NumericOrLetterOrUnderline);
    }

    /// <summary>
    /// 驗證email地址
    /// </summary>
    /// <paramItems name="input">要驗證的字串</paramItems>
    /// <returns>驗證通過返回true</returns>
    public static bool IsEmail(string input)
    {
        return Regex.IsMatch(input, RegularExp.Email);
    }

    /// <summary>
    /// 驗證URL
    /// </summary>
    /// <paramItems name="input">要驗證的字串</paramItems>
    /// <returns>驗證通過返回true</returns>
    public static bool IsUrl(string input)
    {
        return Regex.IsMatch(input, RegularExp.Url);
    }

    /// <summary>
    /// 驗證電話號碼
    /// </summary>
    /// <paramItems name="input">要驗證的字串</paramItems>
    /// <returns>驗證通過返回true</returns>
    public static bool IsTelephone(string input)
    {
        return Regex.IsMatch(input, RegularExp.Telephone);
    }

    /// <summary>
    /// 驗證手機號碼
    /// </summary>
    /// <paramItems name="input">要驗證的字串</paramItems>
    /// <returns>驗證通過返回true</returns>
    public static bool IsMobile(string input)
    {
        return Regex.IsMatch(input, RegularExp.Mobile);
    }

    /// <summary>
    /// 通過檔副檔名驗證圖像格式
    /// </summary>
    /// <paramItems name="input">要驗證的字串</paramItems>
    /// <returns>驗證通過返回true</returns>
    public static bool IsImageFormat(string input)
    {
        return Regex.IsMatch(input, RegularExp.ImageFormat);
    }

    /// <summary>
    /// 驗證IP
    /// </summary>
    /// <paramItems name="input">要驗證的字串</paramItems>
    /// <returns>驗證通過返回true</returns>
    public static bool IsIP(string input)
    {
        return Regex.IsMatch(input, RegularExp.IP);
    }
    /// <summary>
    /// 驗證發票號碼
    /// </summary>
    /// <paramItems name="input">要驗證的字串</paramItems>
    /// <returns>驗證通過返回true</returns>
    public static bool IsInvoice(string input)
    {
        return Regex.IsMatch(input, RegularExp.Invoice);
    }
    /// <summary>
    /// 驗證日期（YYYY-MM-DD）
    /// </summary>
    /// <paramItems name="input">要驗證的字串</paramItems>
    /// <returns>驗證通過返回true</returns>
    public static bool IsDate(string input)
    {
        return Regex.IsMatch(input, RegularExp.Date);
    }

    /// <summary>
    /// 驗證日期和時間（YYYY-MM-DD HH:MM:SS）
    /// </summary>
    /// <paramItems name="input">要驗證的字串</paramItems>
    /// <returns>驗證通過返回true</returns>
    public static bool IsDateTime(string input)
    {
        return Regex.IsMatch(input, RegularExp.DateTime);
    }
    /// <summary>
    /// 驗證顏色（#ff0000）
    /// </summary>
    /// <paramItems name="input">要驗證的字串</paramItems>
    /// <returns>驗證通過返回true</returns>
    public static bool IsColor(string input)
    {
        return Regex.IsMatch(input, RegularExp.Color);
    }
    /// <summary>
    /// MakePlainText 去除HTML TAG
    /// </summary>
    /// <paramItems name="input">要驗證的字串</paramItems>
    /// <returns>回傳文字</returns>
    public static string MakePlainText(string input)
    {
        System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex(@"<[^>]*>");
        return regex.Replace(input, "");
    }
    /// <summary>
    /// getHtmlBody 取得HTML BODY內的CODE
    /// </summary>
    /// <paramItems name="input">要驗證的字串</paramItems>
    /// <returns>回傳文字</returns>
    public static string getHtmlBody(string input)
    {
        System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex(RegularExp.htmlBodyTxt, RegexOptions.IgnoreCase | RegexOptions.Singleline);

        Match body = regex.Match(input);
        return body.Groups["body"].ToString();
    }
    /// <summary>
    /// getImgSrc 取得HTML內的圖片路徑
    /// </summary>
    /// <paramItems name="input">要驗證的字串</paramItems>
    /// <returns>回傳文字</returns>
    public static ArrayList getImgSrc(string input)
    {
        System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex(RegularExp.imgSRC);
        ArrayList imgs = new ArrayList();
        MatchCollection matchs = regex.Matches(input);
        foreach (System.Text.RegularExpressions.Match mat in matchs)
        {
            imgs.Add(mat.Groups["Url"].ToString());
        }
        return imgs;
    }
    /// <summary>
    /// 取代特定文字
    /// </summary>
    public static string MakeReplace(string input, string matchs)
    {
        return Regex.Replace(input, "(?i)(" + matchs + ")", "<span class='sp_b1'>$1</span>");
    }
    /// <summary>
    /// 取代連結顏色樣式
    /// </summary>
    public static string ReplaceAColor(string input)
    {
        return Regex.Replace(input, "(?i)(<a)", "<a style='text-decoration:underline; color:#999'");
    }
    /// <summary>
    /// 特殊換行
    /// </summary>
    public static string MakeNewLine(string input)
    {
        return Regex.Replace(input, "(?<!\r)\n", "<br />");
    }
    /// <summary>
    /// 取代連結
    /// </summary>
    public static string MakeHyperLink(string input)
    {
        Regex urlRegex = new Regex(@"(http:\/\/([\w.]+\/?)\S*)", RegexOptions.IgnoreCase | RegexOptions.Compiled);
        Regex emailRegex = new Regex(@"([a-zA-Z_0-9.-]+\@[a-zA-Z_0-9.-]+\.\w+)", RegexOptions.IgnoreCase | RegexOptions.Compiled);
        input = urlRegex.Replace(input, "<a href=\"$1\" target=\"_blank\">$1</a>");
        input = emailRegex.Replace(input, "<a href=\"mailto:$1\">$1</a>");
        return input;
    }
    /// <summary>
    /// getYoutubeId 回傳
    /// </summary>
    public static string getYoutubeId(string input)
    {
        string output = string.Empty;

        if (input != "")
        {
            if (input.IndexOf("iframe") >= 0)
            {
                Regex regexPattern = new Regex(@"src=\""\S+/embed/(?<videoId>\w+)");
                Match videoIdMatch = regexPattern.Match(input);
                if (videoIdMatch.Success)
                    output = videoIdMatch.Groups["videoId"].Value;
                else
                    output = input;

            }
            else
            {
                Regex tubeUrl = new Regex(@"(?:youtube\.com\/(?:[^\/]+\/.+\/|(?:v|ei(?:mbed)?)\/|.*[?&]v=)|youtu\.be\/)([^""&?\/ ]{11})", RegexOptions.IgnoreCase | RegexOptions.Multiline);
                Match tubeMatch = tubeUrl.Match(input);
                if (tubeMatch.Success)
                    output = tubeMatch.Groups[1].Value;
                else
                    output = input;
            }
        }

        return output;
    }
    /// <summary>
    /// getIframeUrl 回傳
    /// </summary>
    public static string getIframeUrl(string input)
    {
        string output = string.Empty;

        if (input != "")
        {
            Regex tubeUrl = new Regex(@"<iframe [^>]*src=[""']?([^>^""^']+)[""']?[^>]*>", RegexOptions.IgnoreCase | RegexOptions.Multiline);
            Match tubeMatch = tubeUrl.Match(input);
            if (tubeMatch.Success)
                output = tubeMatch.Groups[1].Value;
            else
                output = input;
        }

        return output;
    }
    /// <summary>
    /// 取代SQL Injection關鍵字
    /// </summary>
    /// <paramItems name="input">要驗證的字串</paramItems>
    /// <returns>回傳文字</returns>
    public static string MakeInjection(string input)
    {
        if (input.Length == 0)
            return string.Empty;
        else
            return Regex.Replace(input, RegularExp.InputInjection, string.Empty, RegexOptions.IgnoreCase);
    }
}

public struct RegularExp
{
    public const string Chinese = @"^[\u4E00-\u9FA5\uF900-\uFA2D]+$";
    public const string Color = "^#[a-fA-F0-9]{6}";
    public const string Date = @"^((((1[6-9]|[2-9]\d)\d{2})-(0?[13578]|1[02])-(0?[1-9]|[12]\d|3[01]))|(((1[6-9]|[2-9]\d)\d{2})-(0?[13456789]|1[012])-(0?[1-9]|[12]\d|30))|(((1[6-9]|[2-9]\d)\d{2})-0?2-(0?[1-9]|1\d|2[0-8]))|(((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))-0?2-29-))$";
    public const string DateTime = @"^((((1[6-9]|[2-9]\d)\d{2})-(0?[13578]|1[02])-(0?[1-9]|[12]\d|3[01]))|(((1[6-9]|[2-9]\d)\d{2})-(0?[13456789]|1[012])-(0?[1-9]|[12]\d|30))|(((1[6-9]|[2-9]\d)\d{2})-0?2-(0?[1-9]|1\d|2[0-8]))|(((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))-0?2-29-)) (20|21|22|23|[0-1]?\d):[0-5]?\d:[0-5]?\d$";
    public const string Email = @"^[\w-]+(\.[\w-]+)*@[\w-]+(\.[\w-]+)+$";
    public const string Float = @"^(-?\d+)(\.\d+)?$";
    public const string ImageFormat = @"\.(?i:jpg|bmp|gif|ico|pcx|jpeg|tif|png|raw|tga)$";
    public const string Integer = @"^-?\d+$";
    public const string IP = @"^(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])$";
    public const string Invoice = "^[a-zA-Z]{2}[0-9]{8}$";
    public const string Letter = "^[A-Za-z]+$";
    public const string LowerLetter = "^[a-z]+$";
    public const string MinusFloat = @"^(-(([0-9]+\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\.[0-9]+)|([0-9]*[1-9][0-9]*)))$";
    public const string MinusInteger = "^-[0-9]*[1-9][0-9]*$";
    public const string Mobile = "^0{0,1}13[0-9]{9}$";
    public const string NumbericOrLetterOrChinese = @"^[A-Za-z0-9\u4E00-\u9FA5\uF900-\uFA2D]+$";
    public const string Numeric = "^[0-9]+$";
    public const string NumericOrLetter = "^[A-Za-z0-9]+$";
    public const string NumericOrLetterOrUnderline = @"^\w+$";
    public const string PlusFloat = @"^(([0-9]+\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\.[0-9]+)|([0-9]*[1-9][0-9]*))$";
    public const string PlusInteger = "^[0-9]*[1-9][0-9]*$";
    public const string Telephone = @"(\d+-)?(\d{4}-?\d{7}|\d{3}-?\d{8}|^\d{7,8})(-\d+)?";
    public const string UnMinusFloat = @"^\d+(\.\d+)?$";
    public const string UnMinusInteger = @"\d+$";
    public const string UnPlusFloat = @"^((-\d+(\.\d+)?)|(0+(\.0+)?))$";
    public const string UnPlusInteger = @"^((-\d+)|(0+))$";
    public const string UpperLetter = "^[A-Z]+$";
    public const string Url = @"^http(si)?://([\w-]+\.)+[\w-]+(/[\w- ./?%&=]*)?$";
    public const string InputInjection = @"\b(exec(ute)?|select|update|insert|delete|drop|create|script|iframe)\b|[;']|(-{2})|(/\*.*\*/)";
    public const string htmlBodyTxt = @"<\si*body[^>]*>(?<body>(.*))<[\si/]*body[^>]*>";
    public const string imgSRC = @"(?<=img+.+src\=[\x27\x22])(?<Url>[^\x27\x22]*)(?=[\x27\x22])";

}
