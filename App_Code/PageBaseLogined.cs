﻿using System;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.Odbc;
using System.Text.RegularExpressions;

/// <summary>
/// PageBaseLogined 的摘要描述
/// </summary>
public class PageBaseLogined : PageBase
{
	public PageBaseLogined()
	{
		//
		// TODO: 在這裡新增建構函式邏輯
        //
        Load += new EventHandler(CheckUserLogined);
	}
    protected void CheckUserLogined(object sender, EventArgs e) {
        if (Session[memberSessionKey] == null || String.IsNullOrEmpty(Session[memberSessionKey].ToString())) {
            gotoMsgURL("請重新登入會員!!", "/login.aspx");
        }
    }
}