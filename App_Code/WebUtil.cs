﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Text;
using System.Security.Cryptography;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Text.RegularExpressions;
using System.Collections; 

using System.Net;

 
/// <summary>
/// 公用程序基本模組
/// </summary>
public class WebUtil {

	/// <summary>
	/// 建構函式
	/// </summary>
	public WebUtil() {
		//
		// TODO: 在此加入建構函式的程式碼
		//
	}


	/// <summary>
	/// 轉換圖檔大小
	/// </summary>
	/// <param name="orgStream">檔案 Stream</param>
	/// <param name="newWidth">寬度, pixel</param>
	/// <returns></returns>
	public static Stream ResizeImage(Stream orgStream, int newWidth) {

		Bitmap bitmap = new Bitmap(orgStream);

		// Calculate the new image dimensions
		int origWidth = bitmap.Width;
		int origHeight = bitmap.Height;
		double sngRatio = 1.0 * origWidth / origHeight;
		int newHeight = Convert.ToInt32(1.0 * newWidth / sngRatio);
		if (origWidth < newWidth) {
			newWidth = origWidth;
			newHeight = origHeight;
		}

		// Create a new bitmap which will hold the previous resized bitmap
		Bitmap newBMP = new Bitmap(bitmap, newWidth, newHeight);

		// Create a graphic based on the new bitmap
		Graphics oGraphics = Graphics.FromImage(newBMP);

		// Set the properties for the new graphic file
		oGraphics.SmoothingMode = SmoothingMode.AntiAlias;
		oGraphics.InterpolationMode = InterpolationMode.HighQualityBicubic;

		// Draw the new graphic based on the resized bitmap
		oGraphics.DrawImage(bitmap, 0, 0, newWidth, newHeight);

		System.Drawing.Image img = (System.Drawing.Image)newBMP;
		Stream ret;
		using (MemoryStream stream = new MemoryStream()) {
			img.Save(stream, bitmap.RawFormat);
			stream.Close();
			ret = (Stream)stream;
		}

		// Once finished with the bitmap objects, we deallocate them.
		bitmap.Dispose();
		newBMP.Dispose();
		oGraphics.Dispose();
		return ret;

	}

	/// <summary>
	/// MD5加密
	/// </summary>90
	/// <param name="source"></param>
	/// <returns></returns>
	public static string md5Encode(string source) {

		using (MD5 md5Hash = MD5.Create()) {

			// Convert the input string to a byte array and compute the hash. 
			byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(source));

			// Create a new Stringbuilder to collect the bytes 
			// and create a string.
			StringBuilder sBuilder = new StringBuilder();

			// Loop through each byte of the hashed data  
			// and format each one as a hexadecimal string. 
			for (int i = 0; i < data.Length; i++) {
				sBuilder.Append(data[i].ToString("x2"));
			}

			// Return the hexadecimal string. 
			return sBuilder.ToString();

		}

	}

	/// <summary>
	/// 取得 UUID
	/// </summary>
	/// <returns></returns>
	public static string uuid() {
		return System.Guid.NewGuid().ToString().Replace("-", "");
	}

	/// <summary>
	/// 將系統的 newline 換成 <br>
	/// </summary>
	/// <param name="s"></param>
	/// <returns></returns>
	public static string nl2br(string s) {
		return s.Replace(System.Environment.NewLine, "<br/>");
	}

    public static string addSlashes(string InputTxt) {
        // List of characters handled:
        // \000 null
        // \010 backspace
        // \011 horizontal tab
        // \012 new line
        // \015 carriage return
        // \032 substitute
        // \042 double quote
        // \047 single quote
        // \134 backslash
        // \140 grave accent

        string Result = InputTxt;

        try {
            Result = System.Text.RegularExpressions.Regex.Replace(InputTxt, @"[\000\010\011\012\015\032\042\047\134\140]", "\\$0");
        }
        catch (Exception Ex) {
            // handle any exception here
            Console.WriteLine(Ex.Message);
        }

        return Result;
    }

	#region "PAGE -> 頁面相關"
	#region "setQUERY 接值防呆"
	///<summary>
	///setQUERY 接值防呆 
	///</summary>
	public static string setQUERY(string paramName, params object[] defaultValue) {
		string returnValue = string.Empty;

		if (!string.IsNullOrEmpty(HttpContext.Current.Request[paramName]))
			//returnValue = validatorUtil.MakeInjection(HttpContext.Current.Server.UrlDecode(HttpContext.Current.Request[paramName].ToString()));
            returnValue = validatorUtil.MakeInjection(HttpContext.Current.Server.HtmlEncode(HttpContext.Current.Server.UrlDecode(HttpContext.Current.Request[paramName].ToString())));
		else
			returnValue = (defaultValue.Length > 0 ? defaultValue[0].ToString() : null);

		return returnValue;
	}
	#endregion
	#region "getChildRelation DataSet繫結"
	/// <summary>
	/// getChildRelation DataSet繫結
	/// </summary>
	public static DataView getChildRelation(object dataItem, string relation) {
		DataRowView drv = (DataRowView)dataItem;
		if (drv != null) {
			return drv.CreateChildView(relation);
		} else {
			return null;
		}
	}
	#endregion
	#endregion

	#region "STRING -> 字串相關"
	#region "stringPad 取文字片段"
	/// <summary>
	/// stringPad 取文字片段
	/// </summary>        
	/// <param name="cParam">0:L, R, D(+...), 不傳預設為L, 1: bool(remove html tag)</param>
	public static string stringPad(object xValue, int xLen, params object[] cParam) {
		string inValue = (xValue == DBNull.Value ? "" : (string)xValue);
		string xAward = (cParam.Length == 0 ? "L" : cParam[0].ToString());
		string outValue = string.Empty;
		if (!string.IsNullOrEmpty(inValue)) {
			if (cParam.Length > 1) xValue = validatorUtil.MakePlainText(inValue);
			int xMax = (inValue.Length > xLen ? xLen : inValue.Length);
			if (xAward != "R") {
				outValue = inValue.Substring(0, xMax);
				if (xAward == "D") {
					if (inValue.Length > xLen) outValue += "...";
				}
			} else {
				outValue = inValue.Substring(inValue.Length - xMax, xMax);
			}
		}
		return outValue;
	}
	#endregion

	#region "strPos 取某字元個數"
	/// <summary>
	/// strPos 取某字元個數
	/// </summary>
	/// <paramItems name="xValue">原始字串</paramItems><paramItems name="p09">計算字串</paramItems>
	public static int strPos(object xValue, object ix) {
		string inValue = (xValue == DBNull.Value ? "" : (string)xValue);
		int outValue = 0;
		if (!string.IsNullOrEmpty(inValue)) {
			string[] b = inValue.Split(Convert.ToChar(ix));
			outValue = b.Length - 1;
		}
		return outValue;
	}
	#endregion
	#endregion

	#region "DATETIME -> 日期相關"
	///<summary>
	///DateInterval 日期區間定義
	///</summary>
	public enum DateInterval {
		Second = 1,
		Minute = 2,
		Hour = 3,
		Day = 4,
		Week = 5,
		Month = 6,
		Quarter = 7,
		Year = 8
	}
	#region "dateDiff 日期函數"
	///<summary>
	///dateDiff 日期函數
	///</summary>
	public static long dateDiff(DateInterval xInterval, object startDate, object endDate) {
		long lngDateDiffValue = 0;

		System.TimeSpan objTimeSpan = new System.TimeSpan((Convert.ToDateTime(endDate)).Ticks - (Convert.ToDateTime(startDate)).Ticks);
		switch (xInterval) {
			case DateInterval.Second:
				lngDateDiffValue = (long)objTimeSpan.TotalSeconds;
				break;
			case DateInterval.Minute:
				lngDateDiffValue = (long)objTimeSpan.TotalMinutes;
				break;
			case DateInterval.Hour:
				lngDateDiffValue = (long)objTimeSpan.TotalHours;
				break;
			case DateInterval.Day:
				lngDateDiffValue = (long)objTimeSpan.TotalDays;
				break;
			case DateInterval.Week:
				lngDateDiffValue = (long)(objTimeSpan.TotalSeconds / (7 * 24 * 60 * 60)); //一個星期7天
				break;
			case DateInterval.Month:
				lngDateDiffValue = (long)(objTimeSpan.TotalSeconds / (30 * 24 * 60 * 60)); //一個月計30天
				break;
			case DateInterval.Quarter:
				lngDateDiffValue = (long)(objTimeSpan.TotalSeconds / (90 * 24 * 60 * 60)); //一季計3月
				break;
			case DateInterval.Year:
				lngDateDiffValue = (long)(objTimeSpan.TotalSeconds / (365 * 24 * 60 * 60));   //一年計365天
				break;
		}
		return (lngDateDiffValue);
	}
	#endregion

	#region "isDate 是否為日期"
	///<summary>
	///isDate 是否為日期
	///</summary>
	public static bool isDate(object thisDate) {
		try {
			DateTime dt = DateTime.Parse(Convert.ToString(thisDate));
			return true;
		} catch {
			return false;
		}
	}
	#endregion
	#region "chkDate 日期判斷"
	///<summary>
	///chkDate 日期判斷
	///</summary>
	public static string chkDate(string startDate, string endDate) {
		DateTime sd = Convert.ToDateTime(startDate);
		DateTime ed = Convert.ToDateTime(endDate);
		string result = "1";
		if (DateTime.Compare(DateTime.Today, sd) < 0) {
			result = "-1";
		}
		if (DateTime.Compare(DateTime.Today, ed) > 0) {
			result = "-1";
		}
		return result;
		//return DateTime.Compare(DateTime.Today, sd).ToString() + "###" + DateTime.Compare(DateTime.Today, ed).ToString();
	}
	#endregion



	#region "getCWeekNM 取得中文星期名"
	///<summary>
	///getCWeekNM 取得中文星期名
	///</summary>
	public static string getCWeekNM(object aDate) {
		string[] wNM = new string[] { "日", "一", "二", "三", "四", "五", "六" };

		int wx = (int)(Convert.ToDateTime(aDate)).DayOfWeek;
		return wNM[wx];

	}
	#endregion


	#endregion

	#region "CONTROL -> 控制項相關"


	#region "BindDDL 下拉選項繫結項"
	/// <summary>
	/// BindDDL 下拉選項繫結
	/// </summary>
	/// <param name="cParam">插入預設選項(0,文字,1值), defaultValue</param>
	public static void BindDDL(DropDownList thisObj, object DataSource, params object[] cParam) {
		thisObj.Items.Clear();
		if (DataSource != null) {
			thisObj.DataSource = DataSource;
			thisObj.DataTextField = "txt";
			thisObj.DataValueField = "sno";
			thisObj.DataBind();
		}

		if (cParam.Length > 0) {  //第一個是否塞入空值,要搭配第二個類型
			thisObj.Items.Insert(0, new ListItem(cParam[0].ToString(), cParam[1].ToString()));
		}
		if (cParam.Length > 2) {  //第二個是預設值			
			if (cParam[2].ToString() != "")
				thisObj.SelectedValue = cParam[2].ToString();
		}
	}
	#endregion
	#endregion

}



public class func {

    //取得json資料
    public static string GetJson(string pURL) {
        string Rtn ="";
        pURL = pURL.Trim();
     
        WebClient myWebClient = new WebClient();
        myWebClient.Headers.Add("Content-Type", "application/x-www-form-urlencoded");
         myWebClient.Headers.Add("Charset", "UTF-8");

      try  {
          //下載
          Byte[]  WebResponse = myWebClient.DownloadData(pURL);
          Rtn = Encoding.Default.GetString(WebResponse);


          return Rtn;

      }
      catch  {
          return "";
      }  
        
    } 

     

	public enum variableTp { integer, str, pageint };

    public static bool isDate(string dd) {
        bool isDate = true;

        try {
            DateTime dt = DateTime.Parse(dd);
        }
        catch {
            isDate = false;
        }
        return isDate;
    }

	public static string getVariables(func.variableTp tp, string str) {
		switch (tp) {
			case variableTp.integer:
				if (string.IsNullOrEmpty(str) || !IsNumeric(str)) {
					return "0";
				} else {
					str = str.Replace(",", "");
					return str;
				}

			case variableTp.str:
				str = str.Replace("'", "''");
				str = str.Replace("<", "");
				str = str.Replace(">", "");
				str = str.Replace("+", "");
				str = str.Replace("=", "");
				return str;

			case variableTp.pageint:
				if (string.IsNullOrEmpty(str) || IsNumeric(str)) {
					return "1";
				} else {
					return str.Replace(",", "");
				}
			default:
				return "";
		}
	}

	public static string Right(string s, int length) {
		length = Math.Max(length, 0);

		if (s.Length > length) {
			return s.Substring(s.Length - length, length);
		} else { return s; }
	}


    public static string Left(string s, int length)
    {
        length = Math.Max(length, 0);

        if (s.Length > length)
        {
            return s.Substring(0, length);
        }
        else { return s; }
    }
     
	public static bool IsNumeric(object Expression) {
		bool isNum;

		// Define variable to collect out parameter of the TryParse method. If the conversion fails, the out parameter is zero.
		double retNum;

		// The TryParse method converts a string in a specified style and culture-specific format to its double-precision floating point number equivalent.
		// The TryParse method does not generate an exception if the conversion fails. If the conversion passes, True is returned. If it does not, False is returned.
		isNum = Double.TryParse(Convert.ToString(Expression), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);


		return isNum;
	}



    public enum DateInterval
    {
        Day,
        DayOfYear,
        Hour,
        Minute,
        Month,
        Quarter,
        Second,
        Weekday,
        WeekOfYear,
        Year
    }

    public static long DateDiff(func.DateInterval intervalType, System.DateTime dateOne, System.DateTime dateTwo)
    {
        switch (intervalType)
        {
            case DateInterval.Day:
            case DateInterval.DayOfYear:
                System.TimeSpan spanForDays = dateTwo - dateOne;
                return (long)spanForDays.TotalDays;
            case DateInterval.Hour:
                System.TimeSpan spanForHours = dateTwo - dateOne;
                return (long)spanForHours.TotalHours;
            case DateInterval.Minute:
                System.TimeSpan spanForMinutes = dateTwo - dateOne;
                return (long)spanForMinutes.TotalMinutes;
            case DateInterval.Month:
                return ((dateTwo.Year - dateOne.Year) * 12) + (dateTwo.Month - dateOne.Month);
            case DateInterval.Quarter:
                long dateOneQuarter = (long)System.Math.Ceiling(dateOne.Month / 3.0);
                long dateTwoQuarter = (long)System.Math.Ceiling(dateTwo.Month / 3.0);
                return (4 * (dateTwo.Year - dateOne.Year)) + dateTwoQuarter - dateOneQuarter;
            case DateInterval.Second:
                System.TimeSpan spanForSeconds = dateTwo - dateOne;
                return (long)spanForSeconds.TotalSeconds;
            case DateInterval.Weekday:
                System.TimeSpan spanForWeekdays = dateTwo - dateOne;
                return (long)(spanForWeekdays.TotalDays / 7.0);
            case DateInterval.WeekOfYear:
                System.DateTime dateOneModified = dateOne;
                System.DateTime dateTwoModified = dateTwo;
                while (dateTwoModified.DayOfWeek != System.Globalization.DateTimeFormatInfo.CurrentInfo.FirstDayOfWeek)
                {
                    dateTwoModified = dateTwoModified.AddDays(-1);
                }
                while (dateOneModified.DayOfWeek != System.Globalization.DateTimeFormatInfo.CurrentInfo.FirstDayOfWeek)
                {
                    dateOneModified = dateOneModified.AddDays(-1);
                }
                System.TimeSpan spanForWeekOfYear = dateTwoModified - dateOneModified;
                return (long)(spanForWeekOfYear.TotalDays / 7.0);
            case DateInterval.Year:
                return dateTwo.Year - dateOne.Year;
            default:
                return 0;
        }
    }
}