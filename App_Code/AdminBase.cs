﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// AdminBase 的摘要描述
/// </summary>
public class AdminBase : PageBase
{
    /// <summary>
    /// 所有後台頁面不需要登入的頁面通通繼承此物件
    /// </summary>
	public AdminBase()
	{
		//
		// TODO: 在此加入建構函式的程式碼
		//

        // onLoad add event handler
        Load += new EventHandler(CheckAdmin);
	}

    /// <summary>
    /// 檢查後台頁面未登入的條件
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void CheckAdmin(object sender, EventArgs e) {
        // add some code here
    }

}
