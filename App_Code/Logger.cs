﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Diagnostics;


/// <summary>
/// Logger 的摘要描述
/// </summary>
public class Logger
{
    static object lockMe = new object();

    /// <summary>
    /// 專門處理文字檔LOG的物件
    /// </summary>
	public Logger()
	{
		//
		// TODO: 在此加入建構函式的程式碼
		//
	}

    /// <summary>
    /// 寫入LOG
    /// </summary>
    /// <param name="sErrMsg">LOG訊息</param>
    /// <param name="path">LOG檔案絕對路徑</param>
    public static void WriteLog(string sErrMsg, string path) {

        try {

            if(path == null || path == "") {
                path = HttpContext.Current.Request.PhysicalApplicationPath + @"log\";
            }

            path += (path.EndsWith(@"\")) ? "" : @"\";

            if(!Directory.Exists(path)) {
                DirectoryInfo di = Directory.CreateDirectory(path);
            }


            lock(lockMe) {
                using(StreamWriter sw =
                        new StreamWriter(path + "log" + String.Format("{0:yyyyMMdd}", System.DateTime.Now) + ".log", true)) {
                    sw.WriteLine(String.Format("{0:yyyy/MM/dd hh:mm:ss} {1} {2}\n", System.DateTime.Now,
                        HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"],
                        sErrMsg.Trim()));
                    sw.Close();
                }
            }

        } catch (Exception e){
            Debug.WriteLine(e.ToString());
        }

    }

    /// <summary>
    /// 寫入LOG
    /// </summary>
    /// <param name="sErrMsg">LOG訊息</param>
    public static void WriteLog(string sErrMsg) {

        try {

            String logDir = System.Web.Configuration.WebConfigurationManager.AppSettings["logDir"];
            if(logDir == null || logDir == "") {
                logDir = HttpContext.Current.Request.PhysicalApplicationPath + @"log\";
            }

            WriteLog(sErrMsg, logDir);

        } catch(Exception e) {
            Debug.WriteLine(e.ToString());
        }

    }
}
