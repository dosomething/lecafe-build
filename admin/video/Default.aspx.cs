﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class admin_video_Default : AdminLoginedBase {
    protected void Page_Load(object sender, EventArgs e) {
        if (HttpContext.Current.Request.HttpMethod == "POST") {
            Video ctrl = new Video(dbconn);
            ctrl.action();
            Response.End();
        }
        else {
            getList();
        }

    }

    protected void getList() {
        Video ctrl = new Video(dbconn);
        rptList.DataSource = ctrl.getList();
        rptList.DataBind();
    }
}