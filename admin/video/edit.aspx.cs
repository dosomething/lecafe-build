﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class admin_video_edit : AdminLoginedBase {
    public DataRow row;
    protected void Page_Load(object sender, EventArgs e) {
        if (HttpContext.Current.Request.HttpMethod == "POST") {
            Video ctrl = new Video(dbconn);
            ctrl.action();
            Response.End();
        }
        else {
            getRow();
        }
    }

    protected void getRow() {
        Video ctrl = new Video(dbconn);
        row = ctrl.getRow();
    }
}