﻿<%@ Page Title="" Language="C#" ValidateRequest="false" MasterPageFile="~/admin/MasterPage.master" AutoEventWireup="true" CodeFile="edit.aspx.cs" Inherits="admin_banner_edit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content">

        <h2 class="content-subhead" id="stacked-form">BANNER管理 - <span id="ContentPlaceHolder1_lbl_title">編輯</span></h2>
        <form id="frm" class="pure-form pure-form-stacked">
            <div style="">
                <div style="display: inline-block;">
                    <button type="submit" class="pure-button notice">儲存</button>
                </div>
                <div style="display: inline-block;">
                    <a href="default.aspx" class="pure-button">回上頁</a>
                </div>
            </div>
            <hr>
            <input type="hidden" name="id" value="<%=row["id"]%>">
            <label>狀態</label>
            <input type="radio" name="publish" value="1" <%=row["publish"].ToString() == "1" ? "checked" : "" %>>啟用
            <input type="radio" name="publish" value="0" <%=row["publish"].ToString() == "0" ? "checked" : "" %>>停用
            
            <label>標題</label>
            <textarea name="title" cols="50" rows="5"><%=row["title"]%></textarea>

            <label>類型</label>
            <input type="radio" name="banner_type" value="0" <%=row["banner_type"].ToString() == "0" ? "checked" : "" %>>照片
            <input type="radio" name="banner_type" value="1" <%=row["banner_type"].ToString() == "1" ? "checked" : "" %>>影片

            <label>上架時間</label>
            <input type="text" class="date" id="starttime" name="starttime" value="<%=Convert.ToDateTime(row["starttime"].ToString()).ToString("yyyy/MM/dd HH:mm")%>">

            <label>下架時間</label>
            <input type="text" class="date" id="endtime" name="endtime" value="<%=Convert.ToDateTime(row["endtime"].ToString()).ToString("yyyy/MM/dd HH:mm")%>">  
            
            <label>youtube影片ID</label>
            <%--<textarea name="video" cols="50" rows="5"></textarea>  --%>
            <input type="text" id="video" name="video" value="">  

            <label>連結 </label>
            <input type="text" class="pure-u-1-2" id="url" name="url" value="">

            <label>照片(1920x1080) </label>
            <input type="hidden" id="pic" name="pic" class="pic" value="">         

        </form>
        <script>
            $(function () {
                $('#menu li.banner').addClass('pure-menu-selected');
                $('#frm').ajaxForm({
                    beforeSubmit: function () {
                        var value = {};
                        value.pic = $('#pic').val();
                        value.url = $('#url').val();
                        value.video = $('input[name="video"]').val();
                        value = JSON.stringify(value);
                        var q = $('#frm').serializeObject();
                        q.method = 'save';
                        q.value = value;
                        $.post(location.href, q, null, 'script');
                        return false;
                    }
                });

                $('.date').datetimepicker({
                    changeMonth: true,
                    changeYear: true,
                    dateFormat: "yy-mm-dd"
                });

                
                
                var value = <%=row["value"]%>;
                for(var k in value) {
                    var v = value[k];
                    $('input[type="text"][name="' + k + '"]').val(v);
                    $('input[type="hidden"][name="' + k + '"]').val(v);
                    $('input[type="checkbox"][value="' + v + '"]').attr('checked',true);
                    $('input[type="radio"][name="' + k + '"][value="' + v + '"]').attr('checked',true);
                    $('select[name="' + k + '"]').val(v);
                    $('textarea[name="' + k + '"]').val(v);
                }

                $('.pic').myCoverUpload({
                    img_url: '<%=baseURL%>upload/',
                    upload_url: '/ashx/upload.ashx',
                    crop_url: '/ashx/crop.ashx',
                    thumbnail_type: '',
                    thumbnail_size: ['250x250']
                });
            })
        </script>
    </div>
</asp:Content>

