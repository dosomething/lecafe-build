﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

public partial class admin_banner_Default : AdminLoginedBase
{
    protected void Page_Load(object sender, EventArgs e) {
        if (HttpContext.Current.Request.HttpMethod == "POST") {
            Banner ctrl = new Banner(dbconn);
            ctrl.action();
            Response.End();
        }
        else {
            getList();
        }

    }

    protected void getList() {
        Banner ctrl = new Banner(dbconn);
        rptList.DataSource = ctrl.getList();
        rptList.DataBind();
    }

    public string json_val(string json, string key) {
        JToken data = JsonConvert.DeserializeObject<JObject>(json);
        return data[key].ToString();
    }
}