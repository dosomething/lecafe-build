﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class admin_site_meta_Default : AdminLoginedBase {
    public DataRow row;
    protected void Page_Load(object sender, EventArgs e) {
        if (Request.HttpMethod == "POST") {
            Response.ContentType = "text/javascript";
            string method = Request.Form["method"];
            switch (method) {
                case "save":
                    do_save();
                    break;
            }

            Response.End();
        }
        else {
            getRow();
        }
    }

    private void do_save() {
        SiteMeta ctrl = new SiteMeta(dbconn);
        ctrl.save();
        Response.Write("alert('儲存完成!');");
    }

    protected void getRow() {
        SiteMeta ctrl = new SiteMeta(dbconn);
        row = ctrl.get();
    }
}