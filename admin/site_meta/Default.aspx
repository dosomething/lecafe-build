﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="admin_site_meta_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content">

        <h2 class="content-subhead" id="stacked-form">網站管理 - <span id="ContentPlaceHolder1_lbl_title">編輯</span></h2>
        <form id="frm" class="pure-form pure-form-stacked">
            <div style="">
                <div style="display: inline-block;">
                    <button type="submit" class="pure-button notice">儲存</button>
                </div>
                <%--<div style="display: inline-block;">
                    <a href="default.aspx" class="pure-button">回上頁</a>
                </div>--%>
            </div>
            <hr>

            <label>網站名稱</label>
            <input type="text" id="site_title" name="site_title" value="<%=row["site_title"]%>">

            <label>網站關鍵字</label>
            <textarea name="site_keyword" rows="5" cols="50"><%=row["site_keyword"]%></textarea>

            <label>網站描述</label>
            <textarea name="site_description" rows="5" cols="50"><%=row["site_description"]%></textarea>

            <label>照片(250x250) </label>
            <input type="hidden" id="site_ogimage" name="site_ogimage" class="pic" value="<%=row["site_ogimage"]%>">
        </form>
        <script>
            $(function () {
                $('#menu li.site_meta').addClass('pure-menu-selected');
                $('#frm').ajaxForm({
                    beforeSubmit: function () {
                        var q = 'method=save&' + $('#frm').formSerialize();
                        $.post(location.href, q, null, 'script');
                        return false;
                    }
                });

                $('.pic').myCoverUpload({
                    img_url: '<%=baseURL%>upload/',
                    upload_url: '/ashx/upload.ashx',
                    crop_url: '/ashx/crop.ashx',
                    thumbnail_type: 'crop',
                    thumbnail_size:['250x250']
                });
            })
        </script>
    </div>
</asp:Content>

