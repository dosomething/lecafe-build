﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_Default : PageBase
{
    protected void Page_Load(object sender, EventArgs e) {
        if (HttpContext.Current.Request.HttpMethod == "POST") {
            Account ctrl = new Account(dbconn);
            int account_id = ctrl.account_login();
            if (account_id == 0) {
                Response.Write("alert('登入失敗')");
            }
            else {
                Session[loginSessionKey] = account_id;
                if (Request.QueryString["path"] == null || String.IsNullOrEmpty(Request.QueryString["path"].ToString())) {
                    Response.Write("location='main.aspx'");
                }
                else {
                    Response.Write("location='" + Request.QueryString["path"] + "'");
                }
            }

            Response.End();
        }

    }
}