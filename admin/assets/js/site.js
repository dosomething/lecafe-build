
//封面上傳
; (function ($) {
    $.fn.myCoverUpload = function (settings, callback) {
        var cropper;
        var _defaultSettings = {
            img_url: 'aaa',
            upload_url: 'aaa',
            crop_url: 'aaa',
            thumbnail_type: 'origin',
            thumbnail_size: [],
            crop_min: [0, 0],
            img_src: ''
        };
        var _settings = $.extend(_defaultSettings, settings);
        var _show_preivew = function (instance, json) {
            var file_name = json.file;
            var img = $('<img src="' + _settings.img_url + file_name + '?r='+Math.floor(Math.random()*10000)+'" style="border:1px solid #cccccc;'+ (_settings.thumbnail_type=='crop'?'':'max-width:100%;') +'">');
            var btn_delete = $('<a href="javascript:void(0)"><i class="fa fa-trash"></i>DELETE</a>');
            instance.parent().prev().val(file_name);
            btn_delete.on('click', function () {
                instance.parent().find('.myCoverUpload_preview').empty();
                instance.parent().prev().val('')
            })
            instance.parent().find('.myCoverUpload_preview')
                .empty()
                .append('<br>')
                .append(img)
                .append('<br>')
                .append(btn_delete)
                .append('<br>')
                .append('<br>')
        }

        var _crop_init = function (instance, json) {
            var arr_size = _settings.thumbnail_size[0].split('x');
            $('body').append('<div id="my_crop" style="width:' + arr_size[0] + 'px;height:' + arr_size[1] + 'px;"></div>');
            var file_name = _settings.img_url + json.file;
            console.log(file_name)
            var cropperOptions = {
                modal: true,
                cropData: {
                    filename: json.file
                },
                cropUrl: _settings.crop_url,
                loadPicture: file_name,
                enableMousescroll: true,
                rotateControls: false,
                outputUrlId: instance.parent().prev().attr('id'),
                onBeforeImgUpload: function () { console.log('onBeforeImgUpload') },
                onAfterImgUpload: function () {
                    var modal_H = $('#croppicModalObj').height();
                    if (modal_H > $(window).height()) {
                        $('#croppicModalObj').css('margin-top', 0);
                    }

                    console.log('onAfterImgUpload');
                    $('.cropControlReset').off('click').on('click', function () {
                        //var cropper = new Croppic('my_crop');
                        cropper.destroy();
                        $('#my_crop').remove();
                    })
                },
                onImgDrag: function () { },
                onImgZoom: function () { },
                onBeforeImgCrop: function () { },
                onAfterImgCrop: function () {
                    var src = instance.parent().prev().val();

                    src = src.replace(_settings.img_url, '');
                    instance.parent().prev().val(src);
                    cropper.destroy();
                    $('#my_crop').remove();

                    json.file = src
                    _show_preivew(instance, json);
                },
                onError: function (errormessage) { console.log('onError:' + errormessage) }
            }

            cropper = new Croppic('my_crop', cropperOptions);
        }
        return this.each(function (idx) {
            var container = $('<div class="myCoverUpload_container"></div>'),
                //file_input = $('<input name="myCoverUpload_instance_' + idx + '" type="file" placeholder="" class="pure-input">'),
                file_input = $('<input name="myImg" type="file" placeholder="" class="pure-input">'),
                loading = $('<span class="myCoverUpload_loading"></span>'),
                preview = $('<div class="myCoverUpload_preview"></div>'),
                arr_size = JSON.stringify(_settings.thumbnail_size);
            container.append(file_input).append(loading).append(preview);
            $(this).after(container);
            file_input.fileupload({
                formData: {
                    img_url: _settings.img_url,
                    thumbnail_type: _settings.thumbnail_type,
                    thumbnail_size: arr_size
                },
                change: function (e, data) {
                    $(this).next().html('<i class="icon-spinner icon-spin"></i>');
                },
                beforeSend: function (e, data) {
                },
                fileuploadsubmit: function (e, data) {
                },
                url: _settings.upload_url,
                sequentialUploads: true,
                dataType: 'json',
                done: function (e, data) {
                    var json = data.result;
                    //console.log(data)
                    if (!json.error) {
                        $(this).next().html('');
                        switch (_settings.thumbnail_type) {
                            case 'crop':
                                _crop_init($(this), json);
                                break;
                            default:
                                _show_preivew($(this), json);
                                break;
                        }
                    } else {
                        alert(json.error);
                    }
                },
                dropZone: null
            });

            if ($(this).val()) {
                _show_preivew(file_input, { file: $(this).val() })
            }
        });
    };
})(jQuery);

; (function ($) {
    $.fn.myGalleryUpload = function (settings) {
        var _defaultSettings = {
            img_url: 'aaa',
            upload_url: 'aaa',
            thumbnail_type: 'origin',
            thumbnail_size: [],
            crop_min: [0, 0],
            img_src: '',
            max_imgs: 6,
            uploaded: 0
        };
        var _settings = $.extend(_defaultSettings, settings);
        var _show_preivew = function (instance, json) {
            var file_name = json.file;
            var img = $('<img src="' + _settings.img_url + file_name + '" style="border:1px solid #cccccc; max-width:140px">');
            var btn_delete = $('<a href="javascript:void(0)"><i class="icon-trash"></i>DELETE</a>');
            instance.parent().prev().val(file_name);
            btn_delete.on('click', function () {
                $(this).parents('.myGalleryUpload_item').remove();
            })

            var img_item = $('<div class="myGalleryUpload_item" style="float:left"></div>');
            img_item
                .append('<input type="hidden" name="gallery[]" value="' + file_name + '">')
                .append('<br>')
                .append(img)
                .append('<br>')
                .append(btn_delete)
                .append('<br>')
                .append('<br>')
            instance.parent().find('.myGalleryUpload_preview').append(img_item)
        }
        return this.each(function (idx) {
            var container = $('<div class="myGalleryUpload_container"></div>'),
                //file_input = $('<input name="myGalleryUpload_instance_' + idx + '" type="file" placeholder="" class="pure-input" multiple>'),
                file_input = $('<input name="myGalleryUpload_instance" type="file" placeholder="" class="pure-input" multiple>'),
                loading = $('<span class="myGalleryUpload_loading"></span>'),
                preview = $('<div class="myGalleryUpload_preview"></div>'),
                arr_size = JSON.stringify(_settings.thumbnail_size),
                html = $(this).html() //已上傳照片
            $(this).html('');
            container.append(file_input).append(loading).append(preview);
            $(this).append(container).append('<div style="clear:both;"></div>');
            file_input.fileupload({
                formData: {
                    thumbnail_type: _settings.thumbnail_type,
                    thumbnail_size: arr_size
                },
                stop: function (e) {
                    console.log('Uploads finished');
                    $(this).next().html('');
                },
                change: function (e, data) {
                    $(this).next().html('<i class="icon-spinner icon-spin"></i>');
                },
                beforeSend: function (e, data) {
                },
                fileuploadsubmit: function (e, data) {
                },
                url: _settings.upload_url,
                sequentialUploads: true,
                dataType: 'json',
                done: function (e, data) {
                    var json = data.result;
                    if (!json.error) {

                        switch (_settings.thumbnail_type) {
                            case 'crop':
                            default:
                                _show_preivew($(this), json);
                                break;
                        }
                    } else {
                        alert(json.error);
                    }
                },
                dropZone: null
            });

            if (html != '') {
                var temp = $('<div>' + html + '</div>');
                temp.find('input[type="hidden"]').each(function () {
                    _show_preivew(file_input, { file: $(this).val() })
                })
                //_show_preivew(file_input,{file:$(this).val()})
            }

            preview.sortable({ placeholder: "sort-grid-highlight" });
        });
    };
})(jQuery);

//樣板模組
; (function ($) {
    $.fn.myTemplate = function (settings) {
        var _defaultSettings = {
            img_url: '',
            upload_url: '',
            crop_url: '',
            view_loader_url: '',
            template_info: [],
            ckcss: ''
        };
        var _settings = $.extend(_defaultSettings, settings);
        var _add_template = function (btn_instance) {
            var language_index = btn_instance.parents('.tab_container').index() - 1,
                template_container = btn_instance.parent().next(),
                template_selected = btn_instance.parent().find('input[type="radio"]:checked').index('.my_template_toolbar:eq(' + language_index + ') input'),
                view_id = btn_instance.parent().find('input[type="radio"]:checked').val(),
                view = _settings.template_info[template_selected].view,
                section_count = template_container.find('.section').length + 1

            $.post(_settings.view_loader_url, 'view_id=' + view_id + '&view=' + view + '&section_idx=' + section_count + '&rand=' + Math.random(), function (data) {
                var section = $(data);
                template_container.append(section);
                _accordion(template_container);
                _set_template_input(section)
            }, 'html')
        }
        var _accordion = function (instance) {
            if (typeof instance.data("ui-accordion") != "undefined") {
                instance.accordion("destroy");
            }
            instance.accordion({
                header: "> div > h3",
                collapsible: true,
                autoHeight: true,
                heightStyle: "content",
                active: instance.find('.section').length - 1,
                activate: function (event, ui) {
                    $(this).find('.template_text').attr('contenteditable', true);
                }
            }).sortable({
                items: "div.section",
                axis: "y",
                handle: 'h3',
                distance: 20,
                scroll: false,
                start: function (event, ui) {
                    var textareaId = ui.item.find('textarea').attr('id');
                    if (typeof textareaId != 'undefined') {
                        var editorInstance = CKEDITOR.instances[textareaId];
                        editorInstance.destroy();
                        CKEDITOR.remove(textareaId);
                    }
                },
                stop: function (event, ui) {
                    var textareaId = ui.item.find('textarea').attr('id');
                    if (typeof textareaId != 'undefined') {
                        //CKEDITOR.replace( textareaId );
                    }
                    _set_template_input(ui.item)
                }
            }).disableSelection;
        }

        var _crop_init = function (instance, json) {
            var img = instance.next();
            var hidden = instance.prev();
            var arr_size = [img.width(), img.height()];
            $('body').append('<div id="my_crop" style="width:' + arr_size[0] + 'px;height:' + arr_size[1] + 'px;"></div>');
            var file_name = json.img_url + json.file;
            //console.log(file_name)
            var cropperOptions = {
                modal: true,
                cropUrl: _settings.crop_url,
                loadPicture: file_name,
                enableMousescroll: true,
                rotateControls: false,
                outputUrlId: hidden.attr('id'),
                onBeforeImgUpload: function () { console.log('onBeforeImgUpload') },
                onAfterImgUpload: function () {
                    console.log('onAfterImgUpload');
                    $('.cropControlReset').off('click').on('click', function () {
                        //var cropper = new Croppic('my_crop');
                        cropper.destroy();
                        $('#my_crop').remove();
                    })
                },
                onImgDrag: function () { },
                onImgZoom: function () { },
                onBeforeImgCrop: function () { },
                onAfterImgCrop: function () {
                    console.log("hidden.val()=" + hidden.val())
                    var src = hidden.val();
                    src = src.replace(_settings.img_url, '');
                    img.attr('src', json.img_url + src);
                    hidden.val(src);
                    cropper.destroy();
                    $('#my_crop').remove();
                },
                onError: function (errormessage) { console.log('onError:' + errormessage) }
            }

            cropper = new Croppic('my_crop', cropperOptions);
        }

        var _set_template_input = function (section) {
            //設定上傳
            section.find('.template_img_upload').css('cursor', 'pointer').on('click', function () {
                var target_img = $(this);
                var instance = target_img.prev()
                instance.fileupload({
                    formData: {
                        width: target_img.width()
                    },
                    change: function (e, data) {
                        //$(this).next().html('<i class="icon-spinner icon-spin"></i>');
                    },
                    beforeSend: function (e, data) {
                    },
                    fileuploadsubmit: function (e, data) {
                    },
                    url: _settings.upload_url,
                    sequentialUploads: true,
                    dataType: 'json',
                    done: function (e, data) {
                        var json = data.result;
                        if (!json.error) {
                            $(this).next().html('');
                            _crop_init($(this), json);
                        } else {
                            alert(json.error);
                        }
                        // if (!json.error) {
                        //     var file_name = json.file;

                        //     target_img.attr('src',json.img_url + json.file)
                        //     target_img.prev().prev().val(json.file);
                        // } else {
                        //     alert(json.error);
                        // }
                    },
                    dropZone: null
                })
                .trigger('click');
            })

            //ck
            var config = new Object();
            config.toolbar = 'Basic';
            config.extraPlugins = 'stylesheetparser';
            //config.extraPlugins = 'autogrow'; //搭配JQUERY UI TAB怪怪的
            config.contentsCss = _settings.ckcss;
            config.stylesSet = [];
            config.autoGrow_onStartup = true;
            config.toolbar_Basic = [
                ['Bold', 'Italic', 'Underline'],
                ['Link', 'Unlink'],
                ['TextColor', 'BGColor']
            ];
            section.find('.template_text textarea').ckeditor(function () { }, config);

            section.find('a.del_template').on('click', function () {
                if (confirm('確定要刪除樣板？') == false) return false;
                var section = $(this).parents('.section');
                //CKEDITOR要一併刪除
                var textarea = section.find('textarea');
                textarea.each(function () {
                    var editorInstance = CKEDITOR.instances[$(this).attr('id')];
                    editorInstance.destroy();
                    console.log($(this).attr('id'));
                })
                var template_container = section.parent();
                section.remove();
                _accordion(template_container);
            })
        }

        return this.each(function (idx) {
            //設定template選項
            var default_value = $(this).html();
            $(this).empty();
            var template_toolbar = $('<div class="my_template_toolbar"></div>');
            $(this).append(template_toolbar);
            for (key = 0; key < _settings.template_info.length; key++) {
                var checked = '';
                if (key == 0) checked = 'checked';
                var input_radio = $('<input type="radio" name="myTemplate_radio_' + idx + '" value="' + _settings.template_info[key].id + '" ' + checked + '>');
                var img = $('<img src="' + _settings.template_info[key].img + '">');
                template_toolbar.append(input_radio).append(img);
            }
            var btn_add = $('<a class="pure-button">新增樣板</a>');
            template_toolbar.append('<br>').append('<br>').append(btn_add);
            btn_add.on('click', function () { _add_template($(this)) })

            //設定accordion
            var template_container = $('<div class="my_template_accordion" style="margin:10px 0"></div>');
            $(this).append(template_container);
            template_container.append(default_value);
            _accordion(template_container);
            _set_template_input(template_container.find('.section'))
        });
    };
})(jQuery);


$.fn.serializeObject = function () {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function () {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};
