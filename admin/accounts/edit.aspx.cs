﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class admin_accounts_edit : AdminLoginedBase
{
    public DataRow row;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (HttpContext.Current.Request.HttpMethod == "POST") {
            Account ctrl = new Account(dbconn);
            ctrl.action();
            Response.End();
        }
        else {
            getRow();
        }
    }

    protected void getRow() {
        Account ctrl = new Account(dbconn);
        row = ctrl.getRow();
    }
}