﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="admin_account_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content">

        <h2 class="content-subhead" id="stacked-form">帳號管理</h2>
        <div class="right">
            <a class="pure-button notice" href="edit.aspx" title="新增">新增</a>
        </div>
        <table class="pure-table pure-table-bordered" width="100%">
            <thead>
                <tr>
                    <th width="15%">帳號啟用</th>
                    <th>使用者帳號</th>
                    <th>使用者姓名</th>
                    <th width="130"></th>
                </tr>
            </thead>
            <tbody class="ui-sortable">
                <asp:Repeater ID="rptList" runat="server">
                    <ItemTemplate>
                        <tr id="row_<%#Eval("id") %>">
                            <td>
                                <div>
                                    <input type="checkbox" class="publish" value="<%#Eval("id") %>" onclick="publish($(this))" <%#Eval("publish").ToString()=="1"?"checked":"" %>>
                                </div>
                            </td>
                            <td><%#Eval("username") %></td>
                            <td><%#Eval("name") %></td>
                            <td>
                                <a class="pure-button" title="編輯" href="edit.aspx?id=<%#Eval("id") %>"><i class="fa fa-pencil"></i></a>
                                <a class="pure-button" title="刪除" onclick="del('<%#Eval("id") %>')"><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>

            </tbody>
        </table>
        <script>
            $(function () {
                $('#menu li.accounts').addClass('pure-menu-selected');

                $('.icon-move').css('visibility', 'hidden');
                $('td div').hover(
                    function () {
                        $(this).find('.icon-move').css('visibility', 'visible');
                    },
                    function () {
                        $(this).find('.icon-move').css('visibility', 'hidden');
                    }
                    );
                $('tbody').sortable({
                    handle: ".icon-move",
                    placeholder: "sortable-placeholder",
                    start: function (event, ui) {
                        $('.sortable-placeholder').append('<td colspan="20">&nbsp;</td>')
                    },
                    stop: function (event, ui) {
                        var q = 'method=publish';
                        $('.icon-move').each(function (index, element) {
                            q += '&id[]=' + $(this).attr('value');
                        });
                        $.post(location.href, q)
                    }
                });
            })
            function publish(obj) {
                var q = "method=publish&id=" + obj.attr('value');
                if (obj.prop("checked")) {
                    q += "&publish=1"
                }
                else {
                    q += "&publish=0"
                }
                $.post(location.href, q, null, 'script');
            }

            function del(id) {
                if (!confirm('Delete?')) return;
                $.post(location.href, 'method=delete&id=' + id, null, 'script');
            }

        </script>
    </div>
</asp:Content>

