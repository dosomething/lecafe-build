﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/MasterPage.master" AutoEventWireup="true" CodeFile="edit.aspx.cs" Inherits="admin_accounts_edit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content">

        <h2 class="content-subhead" id="stacked-form">帳號管理 - <span id="ContentPlaceHolder1_lbl_title">編輯</span></h2>
        <form id="frm" class="pure-form pure-form-stacked">
            <div style="">
                <div style="display: inline-block;">
                    <button type="submit" class="pure-button notice">儲存</button>
                </div>
                <div style="display: inline-block;">
                    <a href="default.aspx" class="pure-button">回上頁</a>
                </div>
            </div>
            <hr>
            <input type="hidden" name="id" value="<%=row["id"]%>">
            <label>狀態</label>
            <input type="radio" name="publish" value="1" <%=row["publish"].ToString() == "1" ? "checked" : "" %>>啟用
            <input type="radio" name="publish" value="0" <%=row["publish"].ToString() == "0" ? "checked" : "" %>>停用
            
            <label>帳號</label>
            <input type="text" id="username" name="username" value="<%=row["username"]%>" <%=row["id"].ToString()=="0"?"":"disabled"%>>

            <label>姓名</label>
            <input type="text" id="name" name="name" value="<%=row["name"]%>">

            <label>新密碼</label>
            <input type="password" id="userpwd" name="userpwd" placeholder="">
            <label>密碼確認</label>
            <input type="password" id="userpwd2" name="userpwd2" placeholder="">
        </form>
        <script>
            $(function () {
                $('#menu li.accounts').addClass('pure-menu-selected');
                $('#frm').ajaxForm({
                    beforeSubmit: function () {
                        if ($("input[name=userpwd]").val() != "" || $("input[name=userpwd2]").val() != "") {
                            if ($("input[name=userpwd]").val() != $("input[name=userpwd2]").val()) {
                                alert("密碼不相同!");
                                return false;
                            }
                        }
                        var q = 'method=save&' + $('#frm').formSerialize();
                        $.post(location.href, q, null, 'script');
                        return false;
                    }
                });
            })
        </script>
    </div>
</asp:Content>

