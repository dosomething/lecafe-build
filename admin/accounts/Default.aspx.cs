﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient; 

public partial class admin_account_Default : AdminLoginedBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (HttpContext.Current.Request.HttpMethod == "POST") {
            Account ctrl = new Account(dbconn);
            ctrl.action();
            Response.End();
        }
        else {
            getList();
        }
    }

    protected void getList() {
        Account ctrl = new Account(dbconn);
        rptList.DataSource = ctrl.getList();
        rptList.DataBind();
    }
}