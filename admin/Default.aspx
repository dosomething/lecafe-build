﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="admin_Default" %>


<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <title>左岸咖啡後台管理系統</title>
    <link rel="stylesheet" href="assets/css/pure-min.css">
    <!--[if lte IE 8]>
        <link rel="stylesheet" href="assets/css/layouts/side-menu-old-ie.css">
    <![endif]-->
    <!--[if gt IE 8]><!-->
    <link rel="stylesheet" href="assets/css/layouts/side-menu.css">
    <!--<![endif]-->
    <link rel="stylesheet" href="assets/css/layouts/customize.css">
    <script src="assets/js/jquery-1.11.0.min.js"></script>
    <script src="assets/js/jquery.form.min.js"></script>
    <script>
        $(function () {
            $('#frm').ajaxForm({
                beforeSubmit: function (a, f, o) {
                    var q = $('#frm').formSerialize();
                    $.post(self.url, q, null, 'script');
                    return false;
                }
            });
        })
    </script>
</head>
<body>
    <div class="pure-g">
        <div class="pure-u-1" style="max-width:300px; margin:0 auto;">
            <form class="pure-form" id="frm" autocomplete="off">
                <legend>左岸咖啡後台管理系統</legend>
                <fieldset class="pure-group">
                    <input type="text" name="username" class="pure-input-1" placeholder="Username" title="Username">
                    <input type="password" name="password" class="pure-input-1" placeholder="Password" title="Password">
                </fieldset>

                <button type="submit" class="pure-button pure-input-1 notice">Sign in</button>
            </form>
        </div>
    </div>
</body>

</html>
