﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class admin_activity_Default : AdminLoginedBase {
    protected void Page_Load(object sender, EventArgs e) {
        if (HttpContext.Current.Request.HttpMethod == "POST") {
            Activity ctrl = new Activity(dbconn);
            ctrl.action();
            Response.End();
        }
        else {
            getList();
        }

    }

    protected void getList() {
        Activity ctrl = new Activity(dbconn);
        rptList.DataSource = ctrl.getList();
        rptList.DataBind();
    }
}