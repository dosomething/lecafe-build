﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class admin_product_edit : AdminLoginedBase {
    public DataRow row;
    protected void Page_Load(object sender, EventArgs e) {
        if (HttpContext.Current.Request.HttpMethod == "POST") {
            Product ctrl = new Product(dbconn);
            ctrl.action();
            Response.End();
        }
        else {
            getRow();
        }
    }

    protected void getRow() {
        Product ctrl = new Product(dbconn);
        row = ctrl.getRow();
    }
}