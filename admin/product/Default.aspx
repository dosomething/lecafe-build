﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="admin_product_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content">

        <h2 class="content-subhead" id="stacked-form">商品管理</h2>
        <div class="right">
            <a class="pure-button notice" href="edit.aspx" title="新增">新增</a>
        </div>
        <table class="pure-table pure-table-bordered" width="100%">
            <thead>
                <tr>
                    <th width="15%">發佈</th>
                    <th>標題</th>
                    <th>上下架</th>
                    <th width="130"></th>
                </tr>
            </thead>
            <tbody class="ui-sortable">
                <asp:Repeater ID="rptList" runat="server">
                    <ItemTemplate>
                        <tr id="row_<%#Eval("id") %>">
                            <td>
                                <div>
                                    <i class="fa fa-arrows"></i>
                                    <input type="checkbox" class="publish" value="<%#Eval("id") %>" onclick="publish($(this))" <%#Eval("publish").ToString()=="1"?"checked":"" %>>
                                </div>
                            </td>
                            <td><%#Eval("title") %></td>
                            <td><%#Convert.ToDateTime(Eval("starttime").ToString()).ToString("yyyy/MM/dd HH:mm") + " ~ " + Convert.ToDateTime(Eval("endtime").ToString()).ToString("yyyy/MM/dd HH:mm") %></td>
                            <td>
                                <a class="pure-button" title="編輯" href="edit.aspx?id=<%#Eval("id") %>"><i class="fa fa-pencil"></i></a>
                                <a class="pure-button" title="刪除" onclick="del('<%#Eval("id") %>')"><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>

            </tbody>
        </table>
        <script>
            $(function () {
                $('#menu li.product').addClass('pure-menu-selected');

                $('tbody').sortable({
                    handle: ".fa-arrows",
                    placeholder: "sortable-placeholder",
                    stop: function (event, ui) {
                        var q = 'method=sort';
                        $('.fa-arrows').each(function (index, element) {
                            q += '&id=' + $(this).next().val();
                        });
                        $.post(location.href, q)
                    }
                });
            })
            function publish(obj) {
                var q = "method=publish&id=" + obj.attr('value');
                if (obj.prop("checked")) {
                    q += "&publish=1"
                }
                else {
                    q += "&publish=0"
                }
                $.post(location.href, q, null, 'script');
            }

            function del(id) {
                if (!confirm('Delete?')) return;
                $.post(location.href, 'method=delete&id=' + id, null, 'script');
            }

        </script>
    </div>
</asp:Content>